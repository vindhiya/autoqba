const mongoose= require('mongoose')
const Schema = mongoose.Schema;

let userSchema = new Schema({
    userid: {
        type: String,
        unique: [true, 'User Id should be unique'],
        //required: [true, 'User Id required'],
      },
    FirstName: {
         type: String,
         minlength:4,
         maxlength:20,
         required:[true,'FirstName Field is required']
    },
    LastName: {
        type: String,
        minlength:4,
        maxlength:20,
        required:[true,'LastName Field is required']
   },
   UserName: {
        type: String,
        required:[true,'FirstName Field is required'],
        unique: true,
        trim: true,
        minlength: 8,
        maxlength:20
    },
    Email: {
        type: String,
        unique: true,
        minlength:4,
        maxlength:35,
        required:[true,'Email is required']
    },
    ConfirmEmail: {
        type: String,
        //required:[true,'ConfirmEmail is required']
    },
    Password: {
        type: String,
        //required:[true,'Password is required']
    },
    systemRole: {
        type: String,
        required:[true,'Role is required']
    },
    CompanyName: {
        type: String,
        minlength:3,
        maxlength:20,
        required:[true,'Company Name is required']
    },
    Status: {
        type: String,
        default: 'Active',
        required:[true,'Status is required']
    },
    Visibility: {
        type: String,
        default:'Private',
        required:[true,'Visibility is required']
    },
    project:{
        type: Array,
        //required: [true, "Project required"]
    },
    Created_at: {
        type: String,
    },
    Updated_at: {
        type: String,
    }
    });

module.exports = mongoose.model('users', userSchema)