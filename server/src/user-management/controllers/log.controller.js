function logger (req, res, next) {
    function afterResponse() {
        res.removeListener('finish', afterResponse);
        res.removeListener('close', afterResponse);
        
        var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        console.log(process.env.NODE_ENV)
        console.log(res.statusCode+" Status, "+req.method+" Method, Request IP: "+ip+", URL : "+ fullUrl+", Token : "+req.headers.token);
    }
    res.on('finish', afterResponse);
    res.on('close', afterResponse);
    next();
}

module.exports = logger;