let mongoose = require('mongoose');
let dateFormat = require("dateformat");
require('../models/user.model');
require('../models/role.model');
require('../../project-admin/models/projects.model');
require('../../common/models/userprojectrole.model')
const Pswd = require('../models/user.model')
let User = mongoose.model('users');
let UserProjectRole = mongoose.model('userprojectrole');
let Role = mongoose.model('roles');
const Project = mongoose.model('Project')
const bcrypt = require('bcrypt');
const saltRounds = 8;
let autoIncrement = require('mongodb-autoincrement');
const jwt = require("jsonwebtoken");
let generator = require('generate-password');
const nodemailer = require("nodemailer");

const db = mongoose.connection
let collectionName = "users";


/*Add New User */
module.exports.newUser = async (req, resp) => {

    let password = generator.generate({
        length: 8,
        numbers: true
    });
    console.log(password);
    bcrypt.hash(password, saltRounds, async function (err, hash) {
        autoIncrement.getNextSequence(db, collectionName, async function (err, autoIndex) {

            let myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
            let created_on = myDate;
            let myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
            let updated_on = myDate;

            let userId = 'User' + autoIndex;
            try {
                let userObjet = {
                    userid: userId,
                    FirstName: req.body.FirstName,
                    LastName: req.body.LastName,
                    UserName: req.body.UserName,
                    Email: req.body.Email,
                    systemRole: req.body.systemRole,
                    CompanyName: req.body.CompanyName,
                    Status: req.body.Status,
                    Visibility: req.body.Visibility,
                    project: req.body.project,
                    Created_at: created_on,
                    Updated_at: updated_on,
                    Password: hash
                }
                User.create(userObjet, function (err, doc) {
                    console.log(100, err, doc);
                    if(!err) {
                        let projects = req.body.project;
                        let projectArr = [];
                        projects.forEach(function(project){
                            let projectDetail = {
                                userid: userId,
                                project_id: project.project_id,
                                roleid: project.Role,
                                status: req.body.status,
                                visibility: req.body.visibility,
                                created_at: created_on,
                                updated_at: updated_on
                            };
                            projectArr.push(projectDetail);
                        });
                        UserProjectRole.create(projectArr, function(err, docs) {
                            console.log(101, err, docs);
                            if(!err) {
                                resp.status(200).json({
                                    "status": "SUCCESS",
                                    "messageCode": "MSG200",
                                    "message": "User created",
                                    "userMessage": "User Added Successfully !!!."
                                });
                                console.log('save');
                                sendEmail(req, resp, password);
                            } else {
                                resp.status(400).json({
                                    "status": "Failure",
                                    "messageCode": "MSG417",
                                    "message": "Error in User Project save",
                                    "userMessage": "Please check your credentials"
                                });
                            }
                        });
                    } else  {
                        resp.status(400).json({
                            "status": "Failure",
                            "messageCode": "MSG416",
                            "message": "Error in User save",
                            "userMessage": "Please check your credentials"
                        });
                    }
                });         
            } catch (err) {
                console.log(err);
                resp.status(400).json({
                    "status": "Failure",
                    "messageCode": "MSG415",
                    "message": "Error in User API",
                    "userMessage": "Please check your credentials"
                });
            }
        });
    });

}


/**Global Function */
function sendEmail(req, resp, password) {
    /* Email Triggering module */
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        secure: false,
        auth: {
            user: 'testmailAutoQBA@gmail.com',
            pass: '123@@456'
        }
    });
    console.log('SMTP Configured');
    let mailOptions = {
        from: 'testmailAutoQBA@gmail.com',
        to: req.body.Email,
        subject: 'Your Password has been changed',
        text: `Dear ` + req.body.UserName + `,` +
            `
This is a confirmation that the password for your account has just been changed.
                
                    UserName : `+ req.body.Email +
            `
                    Password  : ` + password
    }
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return console.log(error);
        } else {
            console.log('Email Sent:  ' + info.response);
        }
    });
}


/**
 * Get User by ID
 */
module.exports.getOneUser = (req, resp) => {
    console.log(req.params.userid);
    let userid = req.params.userid;
    User.findById(userid)
        .exec(function (err, msg) {
            let response = {
                status: 200,
                message: msg
            };
            if (err) {
                response.status = 500;
                response.message = err;
            } else if (!msg) {
                response.status = 404;
                response.message = {
                    "message": "User Id not found " + userid
                };
            }
            resp
                .status(response.status)
                .json(response.message);
        });

};





/**
 * Get users list
 */
module.exports.getAllUsers = (req, resp) => {
    console.log("UserList")
    User.find()
        .then(users => resp.json(users))
        .catch(err => resp.status(400).json('Error:' + err));
};

module.exports.getUserProjectRole = async (req, resp) => {
    console.log(100, "Update User");
    let userid = req.params.userid;

    console.log(101, 'GET userId', userid);
    console.log('UserName from req', req.body);

    await UserProjectRole.find({
        userid: userid
    }, async function (err, data) {
        console.log('dataaaa',data)
        console.log("objecttttttttt", data.length)
        let respData = [];
        let projectData = [];
        let roleData = [];
        if (err) {
            console.log("Error finding User");
            resp
                .status(500)
                .json(err);
            return;
        }
        else if (!data) {
            resp.json({
                "status": "SUCCESS",
                "messageCode": "MSG200",
                "message": "User Project fetched",
                data
            });
        }
        else {
            console.log("In else")
            for (let i = 0; i < data.length; i++) {
                console.log('in loopp')
                let projectId = data[i].project_id;
                let roleId = data[i].roleid;
                let status = data[i].status;
                console.log("projectId", projectId);
                console.log("roleId", roleId);
                console.log("status", status);
                if (status !== 'deleted') {
                    await Project.find({ project_id: projectId }, async function (err, project) {
                        console.log("In common project", project);
                        projectData = project[0].project_name;
                        console.log("projectData", projectData);
                    });
                    await Role.find({ roleid: roleId }, async function (err, role) {
                        roleData = role[0].Role;
                        console.log("In RoleDtaa", roleData);

                        let datas = { project_name: projectData, Role: roleData };
                        respData.push(datas);
                        console.log("responseeeeeee", respData);
                    });
                }
            }
            console.log("responseeeeeeeee", respData);
            resp.json({
                "status": "SUCCESS",
                "messageCode": "MSG200",
                "message": "User Project Role data fetched",
                respData
            });
        }
    })
};



/**
 * Individual User update
 */
module.exports.updateUser = (req, resp) => {
    console.log("Update User");
    let objectId = req.params.userid;

    let updatedOn = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
    let userObject = {
        FirstName: req.body.FirstName,
        LastName: req.body.LastName,
        UserName: req.body.UserName,
        Email: req.body.Email,
        CompanyName: req.body.CompanyName,
        systemRole: req.body.systemRole,
        Status: req.body.Status,
        Visibility: req.body.Visibility,
        Updated_at: updatedOn
    }
    User.findByIdAndUpdate(objectId, userObject, function(err, doc) {
        console.log(103, doc);
        let userId = doc.userid;
        if(!err) {
            let projects = req.body.project;
            console.log(99,projects)
            if(projects!==undefined&&projects!==null&&projects.length>0) {
                UserProjectRole.updateMany({"userid":userId},{"$set": {status: "deleted"}},function(err,docs){
                    console.log(104, docs);
                    UserProjectRole.create()
                    let projectArr = [];
                    projects.forEach(function(project) {
                        let projectObject = {
                            userid:userId,
                            project_id:project.project_id,
                            roleid:project.Role,
                            updated_at: updatedOn
                        }
                        projectArr.push(projectObject);
                    })
                    UserProjectRole.create(projectArr, function(err, docs) {
                        if(!err) {
                            resp.status(200).json({
                                "status": "SUCCESS",
                                "messageCode": "MSG200",
                                "message": "User  updated",
                                "userMessage": "User updated Successfully !!!."
                            });
                        } else {
                            resp.status(404).json({
                                "status": "Failure",
                                "messageCode": "MSG406",
                                "message": "Error while saving new User Project Role entries",
                                "userMessage": "User Id not found"
                            }); 
                        }
                    }) 
                })
            } else {
                resp.status(200).json({
                    "status": "SUCCESS",
                    "messageCode": "MSG200",
                    "message": "User  updated",
                    "userMessage": "User updated Successfully !!!."
                });
            }
        } else  {
            resp.status(404).json({
                "status": "Failure",
                "messageCode": "MSG404",
                "message": "Invalid User Id ",
                "userMessage": "User Id not found"
            }); 
        }
    });
};

/**
 * Delete user
 */
module.exports.deleteUser = (req, resp) => {
    console.log("Delete User");
    let userid = req.params.userid;

    User
        .findByIdAndRemove(userid)

        .exec(function (err, user) {
            if (err) {
                resp
                    .status(404)
                    .json(err);
            }
            else {
                console.log("User deleted, id:", userid);
                resp
                    .status(204)
                    .json({
                        "message": "User Deleted"
                    });
            }
        })
};




/*Change Password */
// module.exports.changePassword = (req, resp) => {
//     console.log(2, "Inside the function ");
//     console.log(req.body.cEmail);
//     console.log(req.body.OldPassword);
//     console.log(req.body.ConfirmPassword);
//         let user = new User({
//             dEmail: req.body.cEmail,
//             OldPassword: req.body.OldPassword,
//             ConfirmPassword:req.body.ConfirmPassword

//         })
//         console.log('in backend');
//         console.log("test",User.OldPassword);

//         User.findOne({

//         }).select()
//             .exec(function (err, pass) {

//                 if (err) {
//                     resp.json({ success: false, message: err });
//                 }
//                 else {
//                     if (!req.body.cEmail) {
//                         resp.json({
//                             "status": "FAILED",
//                             "messageCode": "ERR411",
//                             "message": "No Email was Provided",
//                             "userMessage": "Invalid Credentials , Please check your Username and Password"
//                         });
//                     }
//                     else {
//                         if (pass === null) {
//                             resp.json({
//                                 "status": "FAILED",
//                                 "messageCode": "ERR412",
//                                 "message": "Email was not found",
//                                 "userMessage": "Invalid Credentials , Please check your Username and Password"
//                             });

//                         } else {
//                             console.log("User Value :", req.body.OldPassword)
//                             if(!(req.body.OldPassword)=== undefined){
//                                 console.log("Compared Success");
//                                 // if (bcrypt.compareSync(req.body.OldPassword, pass.Password)) {
//                                 //     console.log("Compared Success");
//                                 // }
//                             }else{
//                                 console.log("Oldpassword undefined");
//                             }
//                             // if (bcrypt.compareSync(user.OldPassword, pass.Password)) {
//                             //     console.log("Success");


//                             // } else {
//                             //     resp.json({
//                             //         "status": "FAILED",
//                             //         "messageCode": "ERR413",
//                             //         "message": "Incorrect Password",
//                             //         "userMessage": "Invalid Credentials , Please check your Username and Password"
//                             //     })
//                             // }
//                         }
//                     }
//                 }
//             });
// };

module.exports.changePassword = (req, resp) => {
    console.log(2, "Inside the function ");
    console.log(req.body.Email);
    console.log(req.body.OldPassword);
    console.log(req.body.ConfirmPassword);

    console.log("test", req.body.OldPassword);

    Pswd.findOne({
        Email: req.body.Email

    }).select()
        .exec(function (err, user) {

            if (err) {
                resp.json({ success: false, message: err });
            }
            else {
                if (!req.body.Email) {
                    resp.json({
                        "status": "FAILED",
                        "messageCode": "ERR411",
                        "message": "No Email was Provided",
                        "userMessage": "Invalid Credentials , Please check your Username and Password"
                    });
                }
                else {
                    if (user === null) {
                        resp.json({
                            "status": "FAILED",
                            "messageCode": "ERR412",
                            "message": "Email was not found",
                            "userMessage": "Invalid Credentials , Please check your Username and Password"
                        });

                    } else {
                        if (bcrypt.compareSync(req.body.OldPassword, user.Password)) {

                            bcrypt.hash(req.body.ConfirmPassword, saltRounds, function (err, hash) {
                                // Store hash in your password DB.
                                console.log("Encrypted Password " + hash);
                                user.Password = hash;

                                user.save(function (err) {
                                    if (err) {
                                        resp.json({
                                            "status": "FAILED",
                                            "messageCode": "ERR500",
                                            "message": "Internal server Error",
                                            "userMessage": "Please try again later"
                                        });
                                    }
                                    else {
                                        resp.json({
                                            "status": "SUCCESS",
                                            "messageCode": "MSG200",
                                            "message": "Password changed ",
                                            "userMessage": "Password changed successfully !!!."
                                        });
                                    }
                                })
                            });

                        } else {
                            resp.json({
                                "status": "FAILED",
                                "messageCode": "ERR413",
                                "message": "Incorrect old Password",
                                "userMessage": "Please enter correct password"
                            })
                        }


                        // user.Password=req.body.ConfirmPassword,

                        // user.save(function(err) {
                        //     if(err){
                        //         resp.json({
                        //             "status": "Failure",
                        //             "messageCode": "MSG416",
                        //             "message": "Enter valid Password",
                        //             "userMessage": "Invalid Data."
                        //             });
                        //     }
                        //     else {
                        //         resp.json({
                        //             "status": "SUCCESS",
                        //             "messageCode": "MSG200",
                        //             "message": "Password updated",
                        //             "userMessage": "Password updated successfully !!!."
                        //             });

                        //     }

                        // });





                        // if (bcrypt.compareSync(req.body.OldPassword, pass.Password)) {
                        //     console.log("Compared Success");
                        // }

                        // if (bcrypt.compareSync(user.OldPassword, pass.Password)) {
                        //     console.log("Success");
                        // } else {
                        //     resp.json({
                        //         "status": "FAILED",
                        //         "messageCode": "ERR413",
                        //         "message": "Incorrect Password",
                        //         "userMessage": "Invalid Credentials , Please check your Username and Password"
                        //     })
                        // }
                    }
                }
            }
        });
};

module.exports.usersUpdateStatus = (req, res) => {
    console.log('inside status update')
    let userid = req.params.userid;   
    User.findById(userid).exec(function (err, doc) {        
       if (doc.Status === "new" || doc.Status === "New") {
         doc.Status = "active";
       }else if (doc.Status === "active"||doc.Status === "Active") {
         doc.Status = "deactive";
       }else if (doc.Status === "inactive" || doc.Status === "Inactive"||doc.Status === "deactive"||doc.Status === "Deactive") {           
         doc.Status = "active";
       }
       doc.save(function (err, usersupdated) {
         console.log(usersupdated)
       });
       console.log('doc status after active', doc.Status);
     });
   };







