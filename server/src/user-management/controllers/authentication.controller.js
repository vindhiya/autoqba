var mongoose = require('mongoose');
require('../models/user.model');
require('../models/role.model');
require('../../common/models/userprojectrole.model')
require('../../project-admin/models/projects.model')

var User = mongoose.model('users');
var Role = mongoose.model('roles');
let UserProjectRole = mongoose.model('userprojectrole')
let Projects = mongoose.model('Project');

const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');
const saltRounds = 8;
const success = {

}

/* Authentication */
module.exports.jwtAuthentication = async (req, resp) => {
    //router.route('/authenticate').post(function (req, resp) {
    let user = new User({
        Email: req.body.Email,
        Password: req.body.Password
    })
    console.log(req.body.Email);

    User.findOne({
        Email: user.Email,
    }).select()
        .exec(function (err, pass) {
            if (err) {
                console.log(5)
                resp.json({ success: false, message: err });
            } else {
                if (!req.body.Email) {
                    resp.json({
                        "status": "FAILED",
                        "messageCode": "ERR411",
                        "message": "No Email was Provided",
                        "userMessage": "Invalid Credentials , Please check your Username and Password"
                    });
                } else {
                    if (pass === null) {
                        resp.json({
                            "status": "FAILED",
                            "messageCode": "ERR412",
                            "message": "Email was not found",
                            "userMessage": "Invalid Credentials , Please check your Username and Password"
                        });

                    } else {
                        if (bcrypt.compareSync(user.Password, pass.Password)) {
                            console.log("Success");
                            const UserName = pass.UserName;
                            console.log("UserName :", pass.UserName);

                            let responseData = [];
                            let userId = pass.userid;

                            console.log(110, userId, responseData);
                            jwt.sign({ user }, '3x6le09r0^p', { expiresIn: '1d' }, (err, token) => {
                                let data = {
                                    "status": "SUCCESS",
                                    "messageCode": "200",
                                    "message": "User Authenticated Successfully",
                                    "userMessage": "Successfully Logged In",
                                    "token": token,
                                    "userName": pass.UserName,
                                    "firstName": pass.FirstName,
                                    "lastName": pass.LastName,
                                    "role": pass.Role,
                                    "email": pass.Email,
                                    "userid": pass.userid
                                };

                                UserProjectRole.find({ userid: userId }, function (err, userProjectList) {
                                    if (!err) {
                                        console.log(102, userProjectList.length, userId);
                                        if (userProjectList === undefined || userProjectList.length === 0) {
                                            resp.json(data);
                                        } else {
                                            for (let i = 0; i < userProjectList.length; i++) {
                                                let userProject = {};
                                                let projectId = userProjectList[i].project_id;
                                                let roleId = userProjectList[i].roleid;
                                                console.log(103, projectId);
                                                console.log(104, roleId);
                                                let userData = [];
                                                Projects.findOne({ project_id: projectId }, function (err, project) {
                                                    if (!err) {
                                                        userProject = {
                                                            ...userProject,
                                                            projectId,
                                                            projectName: project.project_name,
                                                            status: project.status,
                                                            createdBy: project.created_by,
                                                            createdOn: project.created_on
                                                        }
                                                        console.log(105, project);
                                                        let roleData = [];
                                                        Role.findOne({ roleid: roleId }, function (err, role) {
                                                            if (!err) {
                                                                console.log('roleid', roleId);
                                                                console.log('roleName', role);
                                                                userProject = {
                                                                    ...userProject,
                                                                    roleId,
                                                                    roleName: role.Role
                                                                }
                                                                responseData.push(userProject);
                                                                console.log(111, responseData);
                                                                data.projects = responseData
                                                                console.log(112, data);
                                                                resp.json(data);
                                                            } else {
                                                                resp.json(data);
                                                            }
                                                        });
                                                    } else {
                                                        resp.json(data);
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                            });
                        } else {
                            resp.json({
                                "status": "FAILED",
                                "messageCode": "ERR413",
                                "message": "Incorrect Password",
                                "userMessage": "Invalid Credentials , Please check your Username and Password"
                            })
                        }
                    }
                }
            }
        });
};

function check(req, res, next) {
    console.log("In verify");
    console.log("Check : ", req.Role);


}

// /* Verify JWT token */
module.exports.tokenVerification = (req, res) => {
    console.log("In verify")
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            res.json({

                message: 'Token verified...',
                authData
            });
        }
    });
};
