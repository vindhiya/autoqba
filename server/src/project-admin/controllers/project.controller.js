var mongoose = require("mongoose");
var dateFormat = require("dateformat");
console.log("working");
require("../models/projects.model");
var Project = mongoose.model("Project");
var autoIncrement = require("mongodb-autoincrement");
require('../../user-management/models/user.model');
require('../../user-management/models/role.model');
require('../../common/models/userprojectrole.model');
var User = mongoose.model("users");
var UserProjectRole = mongoose.model('userprojectrole');
var Role = mongoose.model('roles');



const db = mongoose.connection;
var collectionName = "projects";

module.exports.addProject = async (req, res) => {
  console.log('inside add');
  var project_name = req.body.project_name;
  var project_desc = req.body.project_desc;
  var visibility = req.body.visibility;
  var user = req.body.user;
  var userscount = user.length;
  var userid = req.body.userid;
  var status = req.body.status;
  var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
  var created_on = myDate;
  var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
  var updated_on = myDate;
  var created_by = req.body.created_by;
  var updated_by = req.body.updated_by;
  var project_admin = req.body.project_admin;

  autoIncrement.getNextSequence(db, collectionName, function (err, autoIndex) {
    let project_id = 'project' + autoIndex;
    let users = req.body.user;
    for (let i = 0; i < users.length; i++) {
      UserProjectRole.find(
        {
          $and: [
            { project_id: project_id },
            { userid: users[i].username },
            { roleid: users[i].userrole }
          ]
        },
        function (err, docs) {
          if (docs.length) {
            res.status(409).json(err);
            console.log("Duplicate user entry");
          }
          else {

            Project.create(
              {
                // let project = new Project({
                project_id: project_id,
                project_name: project_name,
                project_desc: project_desc,
                project_admin: project_admin,
                visibility: visibility,
                user: user,
                userid: userid,
                userscount: userscount,
                status: status,
                created_on: created_on,
                updated_on: updated_on,
                created_by: created_by,
                updated_by: updated_by
              })
            try {
              UserProjectRole.create(
                {
                  // let userproject = new UserProjectRole({
                  project_id: project_id,
                  userid: users[i].username,
                  roleid: users[i].userrole,
                  created_at: created_on,
                  updated_at: updated_on,
                })
              res.status(201).json({ "status": "SUCCESS", });
            }
            catch (err) {
              console.log(err)
            }
          }
        });
    };
  });
};
module.exports.getAllProject = (req, res) => {
  Project.find(function (err, project) {
    if (err) {
      console.log(err);
    } else {
      res.json(project);
    }
  });
};

module.exports.projectsGetOne = (req, res) => {
  console.log("GET PROJECT");
  console.log(req.params.project_id);
  var projectsId = req.params.project_id;
  console.log(projectsId);
  Project.find({ project_id: projectsId }).exec(function (err, doc) {
    var response = {
      status: 200,
      message: doc
    };
    if (err) {
      response.status = 500;
      response.message = err;
    } else if (!doc) {
      response.status = 404;
      response.message = {
        message: "ProjectId not found " + id
      };
    }
    res.status(response.status).json(response.message);
  });
};

module.exports.projectsUpdateOne = (req, res) => {
  var projectsId = req.params.projectsId;
  console.log(projectsId);
  Project.findById(projectsId).exec(function (err, doc) {
    console.log('targettttttttttttt', doc);
    console.log(doc.project_id);
    let users = req.body.user;
    for (let i = 0; i < users.length; i++) {
      UserProjectRole.find(
        {
          $and: [
            { project_id: doc.project_id },
            { userid: users[i].username },
            { roleid: users[i].userrole }
          ]
        },
        function (err, docs) {
          console.log('statusssss', docs);
          if (docs.length > 0 && docs[0].status === 'active') {
            console.log("Duplicate user entry");
            if (users.length === 1) {
              res.status(409).json(err);
            }

          }
          else {

            Project.findById(projectsId).exec(function (err, project) {
              if (err) {
                res.status(500).json(err);
                return;
              } else if (!project) {
                res.status(404).lson({
                  message: "projectId not found " + projectsId
                });
                return;
              }
              var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
              project.project_name = req.body.project_name;
              project.project_desc = req.body.project_desc;
              project.visibility = req.body.visibility;
              project.user = req.body.user;
              project.userscount = project.user.length;
              project.updated_on = myDate;
              project.updated_by = req.body.updated_by;

              project.save();
              try {
                UserProjectRole.create(
                  {
                    project_id: project.project_id,
                    userid: users[i].username,
                    roleid: users[i].userrole,
                    updated_at: myDate
                  }),
                  res.status(204).json({ "status": "updated" });
              }
              catch (err) { console.log(err) }
            });
          }
        });
    }
  })
}

module.exports.projectDeleteOne = (req, res) => {
  var projectsId = req.params.projectsId;
  Project.findByIdAndRemove(projectsId).exec(function (err, project) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(204).json();
    }
  });
};

module.exports.recentProject = (req, res) => {
  Project.find(function (err, project) {
    if (err) {
      console.log(err);
    } else {
      res.json(project);
    }
  })
    .sort({ $natural: -1 })
    .limit(3);
};

module.exports.filterProject = (req, res) => {
  console.log("filter Project");
  var userId = req.params.userId;
  console.log("selected userId: ", userId);

  Project.find({ userid: userId }, function (err, doc) {
    console.log("Filter Project : ", doc);
    var response = {
      status: 200,
      message: doc
    };
    if (err) {
      response.status = 500;
      response.message = err;
      console.error("ERR : ", err);
    } else if (!doc) {
      response.status = 404;
      response.message = {
        message: "User Id not found " + id
      };
    }
    res.status(response.status).json(response.message);
  });
};

module.exports.projectsUpdatebyStatus = (req, res) => {
  console.log('inside ststus update')
  var projectsId = req.params.projectsId;
  Project.findById(projectsId).exec(function (err, doc) {
    if (doc.status === "new") {
      doc.status = "active";
    } else if (doc.status === "active") {
      doc.status = "deactive";
    } else if (doc.status === "deactive" || doc.status === "invisible") {
      doc.status = "active";
    }
    doc.save(function (err, projectsupdated) {
      console.log(projectsupdated)
    });
    console.log('doc status after active', doc.status);
  });
};

module.exports.filterProjectuserrole = async (req, res) => {
  console.log("filter filterProjectuserrole");
  var projectsId = req.params.projectsId;
  console.log("selected projectsId: ", projectsId);

  await UserProjectRole.find({ project_id: projectsId }, async function (err, doc) {
    console.log('lengthhhhhh', doc.length)
    let responseData = [];
    let userData = [];
    let roleData = [];
    if (!err) {

      for (let i = 0; i < doc.length; i++) {
        let objId = doc[i]._id;
        let userId = doc[i].userid;
        let roleId = doc[i].roleid;
        let status = doc[i].status;
        console.log(status);
        console.log(userId);
        console.log(roleId);
        if (status !== 'deleted') {
          await User.find({ userid: userId }, async function (err, user) {
            console.log(i);
            console.log('userdetails', user);
            userData = user[0].UserName;
            console.log('username', userData);
          });
          await Role.find({ roleid: roleId }, async function (err, role) {
            roleData = role[0].Role;
            console.log('user doc .......', roleData);

            let data = { objId, username: userData, userrole: roleData };
            responseData.push(data);
            console.log('response data', responseData);

          });
        }
      }
      //  res.status(200).json({ "status": "SUCCESS", });
      console.log("pandian", responseData);
      res.status(200).json(responseData);
    }
    else {
      res.status(500).json({ message: err });
      console.error("ERR : ", err);
    }
  })
};


module.exports.projectUpdatebyStatusRepo = (req, res) => {
  var projectId = req;
   console.log("project id from Repository###",req);
   console.log("project ID ===>>>>",projectId);
   var query = ({$and : [{"project_id" : projectId},{"status" : "new"}]} , { $set: { "status" : "active" } } ) ;
  Project.updateMany(query).exec(function (err, doc) {
    console.log("document project update status repo ::: >>>>>>>>>>>",doc);
  });
};

/* DELETE Row */
module.exports.deleteOneuser = (req, res) => {
  console.log("req.params:>>>", req.params);
  var userId = req.params.user_id;
  console.log("user value::::", userId);

  UserProjectRole.findById(userId).exec(function (err, user) {
    if (err) {
      // res.status(404).json(err);
      console.log("errrr");
    } else {
      console.log("user print", user);
      user.status = "deleted";
      // res.status(204).json();
      user.save(function (err, userDeleted) {
        // res.status(204).json();
        console.log("user delted ", userDeleted);
      });
    }
  });
};

