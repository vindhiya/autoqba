var express = require('express');
var router = express.Router();
var repositorycontroller =require('../controllers/repository.controller');
var objectscontroller=require('../controllers/repositoryObject.controller');
console.log("Repository index.route")
// Repository Store routes
  router
  .route('/repository/store')
  .get(objectscontroller.getAllObjects);
  

  router
  .route('/repository/store/:repositoryObjectId')
  .get(objectscontroller.getOneObject)
  .put(objectscontroller.updateOneObject)
  .delete(objectscontroller.deleteOneObject)  

  router
  .route('/repository/store/add')
  .post(objectscontroller.addOneObject);

  router
  .route('/repository/store/filter/:orrefid')
  .get(objectscontroller.filterObject);



//Object Repository routes
router
  .route('/repository')
  .get(repositorycontroller.getAllObjectRepository)

  router
  .route('/repository/add')
  .post(repositorycontroller.addObjectRepository);

  router
  .route('/repository/:repositoryId')
  .get(repositorycontroller.getOneObjectRepository)
 .put(repositorycontroller.updateOneObjectRepository)
 .delete(repositorycontroller.deleteOneObjectRepository)

 router
  .route('/repository/status/:repositoryId')
  .put(repositorycontroller.repositoryUpdatebyStatus)

  module.exports = router;
