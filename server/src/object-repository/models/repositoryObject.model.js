const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let repositoryObjectSchema = new Schema(
  {
    repositObject_id: {
      type: String,
      unique: [true, "object Id should be unique"],
      required: [true, "object Id required"]
    },
    orrefid: {
        type: String,
        required: [true, "rep Id required"]
        // default: '15'
      },
    objectType: {
      type: Array,
      // required: [true, "Object type required"],
      minlength: 2,
      maxlength: 100
    },
    class1: {
      type: String,
      required: [true, "class 1  required"],
      minlength: 2,
      maxlength: 100
    },
    class2: {
      type: String,
      required: [false, "class 2  required"],
      minlength: 2,
      maxlength: 100
    },
    class3: {
      type: String,
      required: [false, "class 3  required"],
      minlength: 2,
      maxlength: 100
    },
    name1: {
      type: String,
      required: [true, "Name 1  required"],
      minlength: 2,
      maxlength: 100
    },
    name2: {
      type: String,
      required: [false, "Name 2  required"],
      minlength: 2,
      maxlength: 100
    },
    name3: {
      type: String,
      required: [false, "Name 3  required"],
      minlength: 2,
      maxlength: 100
    },
    locator: {
      type: String,
      required: [true, "Locator required"],
      minlength: 2,
      maxlength: 100
    },

    locator_value: {
      type: String,
      required: [true, "Locator Value required"],
      minlength: 2,
      maxlength: 100
    },
    screen: {
      type: String,
      required: [true, "Screen Name required"],
      minlength: 2,
      maxlength: 100
    },
    field: {
      type: String,
      required: [true, "Field Name required"],
      minlength: 2,
      maxlength: 100
    },
    status: {
      type: String,
      required: [false, "Status required"],
      default: "active",
      minlength: 2,
      maxlength: 100
    },
    visibility : {
      type: String,
      required: [false, "Visibility required"],
      default: "active"
    },
    created_by: {
      type: String,
      default: "admin",

    },
    created_on: {
      type: String,
      
    },
    updated_by: {
      type: String,
      default: "admin",
      minlength: 2,
      maxlength: 100
    },
    updated_on: {
      type: String,
     
    }
  },
  {
    collection: "repositoryObject"
  }
);

module.exports = mongoose.model("RepositoryObject", repositoryObjectSchema);
