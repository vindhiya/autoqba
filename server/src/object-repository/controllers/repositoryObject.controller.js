var mongoose = require("mongoose");
var dateFormat = require("dateformat");
require("../models/repositoryObject.model");
var RepositoryObjects = mongoose.model("RepositoryObject");
var autoIncrement = require("mongodb-autoincrement");
const db = mongoose.connection;
var collectionName = "repositoryObject";
var repositoryController = require("../controllers/repository.controller");

/* ADD AN OBJECT*/
module.exports.addOneObject = function(req, res) {
  console.log("POST new Repository Object");
  RepositoryObjects.find(
    {
      $and: [
        // { objectType: req.body.objectType },
        { locator: req.body.locator },
        { locator_value: req.body.locator_value },
        { screen: req.body.screen },
        { field: req.body.field }
      ]
    },
    function(err, docs) {
      if (docs.length) {
        res.status(409).json(err);
        console.log("Duplicate entry");
        //  cb('Name exists already',null);
      } else {
        var field = req.body.field;
        var locator = req.body.locator;
        var locator_value = req.body.locator_value;
        var orrefid = req.body.orrefid;
        var objectType = req.body.objectType;
        var screen = req.body.screen;
        var status = req.body.status;
        var visibility = req.body.visibility;
        var class1 = req.body.class1;
        var class2 = req.body.class2;
        var class3 = req.body.class3;
        var name1 = req.body.name1;
        var name2 = req.body.name2;
        var name3 = req.body.name3;
        var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss tt");
        var created_on = myDate;
        var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss tt");
        var updated_on = myDate;
        // var created_by = req.body.created_by;
        // var updated_by = req.body.updated_by;
        autoIncrement.getNextSequence(db, collectionName, function(
          err,
          autoIndex
        ) {
          RepositoryObjects.create(
            {
              repositObject_id: "Object" + autoIndex,
              class1: class1,
              class2: class2,
              class3: class3,
              field: field,
              locator_value: locator_value,
              locator: locator,
              name1: name1,
              name2: name2,
              name3: name3,
              objectType: objectType,
              orrefid: orrefid,
              screen: screen,
              status: status,
              visibility: visibility,
              created_on: created_on,
              updated_on: updated_on
              // created_by: created_by,
              // updated_by: updated_by
            },
            function(err, repositoryObject) {
              if (err) {
                console.log("Error creating repository");
                console.log(err);
                res.status(400).json(err);
              } else {
                console.log("RepositoryObjects created!", repositoryObject);
                res.status(201).json(repositoryObject);
                repositoryController.repositoryUpdatebyStatus(
                  repositoryObject.orrefid
                );
              }
            }
          );
        });
      }
    }
  );
};

/* GET ALL OBJECTS*/
module.exports.getAllObjects = (req, res) => {
  RepositoryObjects.find(function(err, repositoryObject) {
    if (err) {
      res.status(404).json({ message: err });
    } else {
      res.status(200).json(repositoryObject);
    }
  });
};

module.exports.filterObject = (req, res) => {
  var repoId = req.params.orrefid;
  var query = { $and: [{ status: { $ne: "deleted" } }, { orrefid: repoId }] };
  RepositoryObjects.find(query, function(err, doc) {
    console.log("Filter Repo Object : ", doc);
    var response = {
      status: 200,
      message: doc
    };
    if (err) {
      response.status = 500;
      response.message = err;
      console.error("ERR : ", err);
    } else if (!doc) {
      response.status = 404;
      response.message = {
        message: "Repo Id not found " + id
      };
    }
    res.status(response.status).json(response.message);
  });
};

/* GET ONE OBJECT */
module.exports.getOneObject = function(req, res) {
  console.log("Get One Repository");
  // var id = req.params.repository_id;
  var id = req.params.repositoryObjectId;
  console.log("GET objectrepositoryId store", id);

  RepositoryObjects.findById(id).exec(function(err, doc) {
    var response = {
      status: 200,
      message: doc
    };
    if (err) {
      console.log("Error finding repository");
      response.status = 500;
      response.message = err;
    } else if (!doc) {
      console.log(doc);
      console.log("Repository Object Id not found in database", id);
      response.status = 404;
      response.message = {
        message: "Objects not found " + id
      };
    }
    res.status(response.status).json(response.message);
    console.log(doc);
  });
};

/* UPDATE ONE OBJECT*/
module.exports.updateOneObject = function(req, res) {
  console.log("req,body::::::::::",req.body);
  // console.log("req.body[0]", req.body[0].locator )
  RepositoryObjects.find(
    {
      $and: [
        //  { objectType: req.body.objectType },
        { locator: req.body.locator },
        { locator_value: req.body.locator_value },
        { screen: req.body.screen },
        { field: req.body.field }
      ]
    },
    function(err, docs) {
      console.log("pandi : ", docs.length);
      if (docs.length) {
        res.status(409).json(err);
        console.log("Duplicate entry");
      } else {
        var repositoryObjectId = req.params.repositoryObjectId;
        RepositoryObjects.findById(repositoryObjectId).exec(function(
          err,
          repositoryObject
        ) {
          if (err) {
            console.log("Error finding update repository");
            res.status(500).json(err);
            return;
          } else if (!repositoryObject) {
            res.status(404).lson({
              message: "Object Repository Id not found " + repositoryObjectId
            });
            return;
          }
          // repositoryObject.orrefid= req.body.orrefid;
          //  repositoryObject.objectType = req.body[0].objectType;
          var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss tt");
          repositoryObject.class1 = req.body.class1;
          repositoryObject.class2 = req.body.class2;
          repositoryObject.class3 = req.body.class3;
          repositoryObject.name1 = req.body.name1;
          repositoryObject.name2 = req.body.name2;
          repositoryObject.name3 = req.body.name3;

          repositoryObject.locator = req.body.locator;
          repositoryObject.locator_value = req.body.locator_value;
          repositoryObject.screen = req.body.screen;
          repositoryObject.field = req.body.field;
          repositoryObject.status = req.body.status;
          repositoryObject.visibility = req.body.visibility;
          repositoryObject.updated_on = myDate;

          repositoryObject.save(function(err, repositoryObjectUpdated) {
            if (err) {
              console.log("Error Save repository Object", err);
              res.status(500).json(err);
            } else {
              console.log("Save");
              res.status(204).json(repositoryObjectUpdated);
            }
          });
        });
      }
    }
  );
};

/* DELETE ALL OBJECT */
module.exports.deleteOneObject = (req, res) => {
  var repositoryObjectId = req.params.repositoryObjectId;

  RepositoryObjects.findById(repositoryObjectId).exec(function(
    err,
    repositoryStore
  ) {
    if (err) {
      res.status(404).json(err);
    } else {
      repositoryStore.status = "deleted";
      // res.status(204).json();
      repositoryStore.save(function(err, repoStoreDeleted) {
        res.status(204).json();
        console.log("repo delted ", repoStoreDeleted);
      });
    }
  });
};
// //DELETE ONE OBJECT
// module.exports.deleteOneObject = function(req, res) {
//   var repositoryObjectId = req.params.repositoryObjectId;

//   console.log("GET repositoryId", repositoryObjectId);

//   RepositoryObjects.findById(repositoryObjectId)
//     .select("-objrep")
//     .exec(function(err, RepositoryObjects) {
//       if (err) {
//         console.log("Error finding RepositoryObjects");
//         res.status(500).json(err);
//         return;
//       } else if (!repositoryObjectId) {
//         console.log(
//           "ObjectRepositoryId not found in database",
//           repositoryObjectId
//         );
//         res.status(404).lson({
//           message: "RepositoryObjects ID not found " + repositoryObjectId
//         });
//         return;
//       }
//       RepositoryObjects.save(function(err, doc) {
//         if (err) {
//           res.status(500).json(err);
//         } else {
//           doc.status = 'deleted';
//           console.log("repository store delete:::::::", doc);
//           res.status(204).json();
//           console.log("RepositoryObjects Deleted Successfully");
//         }
//       });
//     });
// };
