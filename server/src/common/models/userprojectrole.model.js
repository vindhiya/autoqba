const mongoose = require('mongoose')
const Schema = mongoose.Schema;

let userProjectRole = new Schema({
    userid: {
        type: String,
        //required: [true, 'User Id required'],
    },
    roleid: {
        type: String,
        //required: [true, 'User Id required'],
    },
    project_id: {
        type: String,
        //required: [true, 'User Id required'],
    },
    status: {
        type: String,
        default: 'active',
    },
    visibility: {
        type: String,
        default: 'private',
    },
    created_at: {
        type: String,
    },
    updated_at: {
        type: String,
    }
});

module.exports = mongoose.model('userprojectrole', userProjectRole)