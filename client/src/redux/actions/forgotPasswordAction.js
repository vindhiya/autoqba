import { RESET_PASSWORD } from './types';
import Global from '../../components/layout/Global/Global';
import { async } from 'q';
import fetch from '../../utils/leoFetch'
import axios from '../../utils/leoAxios'
import API_ROOT from '../../utils/api-config'

export const password = (values) => async dispatch => {
    await axios.put(API_ROOT+'/password/resetpassword',values)
       .then(resp => {
           if (resp.data.status === "SUCCESS") {
            Global.alertContent = {
                closable: false,
                message: resp.data.userMessage,
                status: "success"
            }
            dispatch({
                payload: {
                },
                type: RESET_PASSWORD
            })
        }
        else {
            Global.alertContent = {
                closable: true,
                message: resp.data.userMessage,
                status: 'error'
            }
            localStorage.removeItem("token");
            dispatch({
                payload: {
                },
                type: RESET_PASSWORD
            })
        }
       })
};
