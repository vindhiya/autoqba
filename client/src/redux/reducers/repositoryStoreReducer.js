import { NEW_OBJECT_STORE,FETCH_OBJECT_STORE, UPDATE_OBJECT_STORE, DELETE_OBJECT_STORE,FILTER_OBJECT_STORE } from '../actions/types';

const initialState = {
    deleteObjectElement: {},
    filterItems : [],
    objectItem : {}, //add object
    objectItems : [], //fetch object
    updateObjectElement : {},

}

export default function (state = initialState, action) {
    console.log(555, action.type,action.payload);
    switch (action.type) {
      case NEW_OBJECT_STORE:
        console.log("inside new object reducer")
        return {
          ...state,
          objectItem: action.payload
        };
        case FETCH_OBJECT_STORE:
      return {
        ...state,
        objectItems: action.payload
      };
      case FILTER_OBJECT_STORE:
      return {
        ...state,
        filterItems: action.payload
      };
      case UPDATE_OBJECT_STORE:
      return {
        // ...state,
        updateObjectElement: action.payload
      };
      case DELETE_OBJECT_STORE:
        // const _id = action.payload._id;
        return {
          ...state,
          deleteObjectElement : action.payload
        };
      default:
        return state;
    }
  }
