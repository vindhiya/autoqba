import {
  FETCH_TOOL,
  NEW_TOOL,
  EDIT_TOOL,
  UPDATE_TOOL,
  DELETE_TOOL,
  FILTER_TOOL
} from "../actions/types";

const initialState = {
  app: {},
  ele: {},
  element: [],
  item: {},
  items: []
};

export default function(state = initialState, action) {
  console.log(action.type, action.payload);
  switch (action.type) {
    case NEW_TOOL:
      return {
        ...state,
        item: action.payload
      };
    case FETCH_TOOL:
      return {
        // ...state,
        items: action.payload
      };
    case EDIT_TOOL: {
      let data = [];
      if (action.payload !== undefined) {
        data = action.payload;
      }
      console.log(44, data);
      return {
        // ...state,
        element: data
      };
    }
    case UPDATE_TOOL:
      return {
        ele: action.payload
      };
    case DELETE_TOOL:
      return {
        ...state,
        app: action.payload
      };
    case FILTER_TOOL:
      return {
        ...state,
        items: action.payload
      };
    default:
      return state;
  }
}
