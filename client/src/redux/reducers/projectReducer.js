import { FETCH_PROJECT, NEW_PROJECT, UPDATE_PROJECT , DELETE_PROJECT, FILTER_PROJECT, FILTER_USERROLE, DELETE_SINGLE_USER, INMEMORY_ADD_USER} from "../actions/types";

const initialState = {
  app: {},
  ele: {},
  item: {},
  items: [],
  singleuser: {},
  userrole: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case NEW_PROJECT:
      return {
        ...state,
        item: action.payload
      };
    case FETCH_PROJECT:
      return {
        // ...state,
        items: action.payload
      };
      case UPDATE_PROJECT:
        return {
          // ...state,
          ele: action.payload
        };
      case DELETE_PROJECT:
        // const _id = action.payload._id;
        return {
          ...state,
          app: action.payload
        };
        case FILTER_PROJECT:
         return {
        ...state,
        items: action.payload
      };
      case FILTER_USERROLE:
         return {
        ...state,
        userrole: action.payload
      };
      case INMEMORY_ADD_USER:
      console.log(222,action)
        return {
          ...state,
          userrole: action.payload
        };
      case DELETE_SINGLE_USER:
        return {
          ...state,
          singleuser: action.payload
        }
    default:
      return state;
  }
}
