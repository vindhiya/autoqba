import {LOGIN,LOGOUT} from '../actions/types';

const initialState = {
  Email: null,
  FirstName: null,
  LastName: null,
  isLoggedIn: false,
  redirect: false,
  token: null,
  userid: null,
}

export default function(state = initialState, action) {
    switch(action.type) {
        case LOGIN:
            return {
                ...action.payload
            }
        case LOGOUT:
            return {
                ...action.payload
            }
        default:
                return state;
    }
}
