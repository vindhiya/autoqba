import { EDIT_APPLICATION } from "../actions/types";

const initialState = {
  appName : '',
  element: {}
};

export default function reducer(state = initialState, action) {
  console.log(action.type, action.payload);
  switch (action.type) {
    case EDIT_APPLICATION: {
      let data = [];
      if (action.payload !== undefined) {
        data = action.payload;
      }
      console.log(44, data);
      return {
        // ...state,
        appName: data.application_name,
        element: data
      };
    }
    default:
      return state;
  }
}