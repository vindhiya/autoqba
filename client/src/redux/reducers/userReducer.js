import {
  FETCH_USER,
  NEW_USER,
  EDIT_USER,
  UPDATE_USER,
  DELETE_USER,
  CHANGE_PASSWORD,
  GET_USERPROJECT
} from "../actions/types";

const initialState = {
  data: {},
  datas: [],
  element: [],
  item: {},
  items: {},
  pass: {},
  userproject: []
};

export default function(state = initialState, action) {
  console.log("in reducer");
  switch (action.type) {
    case NEW_USER:
      console.log("added new user");
      return {
        ...state,
        data: action.payload
      };
    case FETCH_USER:
      return {
        ...state,
        datas: action.payload
      };
    case EDIT_USER: {
      let data = [];
      if (action.payload !== undefined) {
        data = action.payload;
      }
      return {
        ...state,
        element: data
      };
    }
    case GET_USERPROJECT: {
      return {
        ...state,
        userproject: action.payload
      };
    }
    case UPDATE_USER:
      return {
        item: action.payload
      };
    case DELETE_USER:
      return {
        ...state,
        items: action.payload
      };
    case CHANGE_PASSWORD:
      console.log("added password");
      return {
        ...state,
        pass: action.payload
      };
    default:
      return state;
  }
}
