import {
  FETCH_APPLICATION,
  NEW_APPLICATION,
  EDIT_APPLICATION,
  UPDATE_APPLICATION,
  DELETE_APPLICATION,
  FILTER_APPLICATION,
  FILTER1_APPLICATION
} from "../actions/types";

const initialState = {
  app: {},
  ele: {},
  element: {},
  item: {},
  items: [],
  versionFilter: []
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case NEW_APPLICATION:
      console.log("inside new app reducer");
      return {
        ...state,
        item: action.payload
      };
    case FETCH_APPLICATION:
      return {
        // ...state,
        items: action.payload
      };
    case EDIT_APPLICATION: {
      let data = [];
      if (action.payload !== undefined) {
        data = action.payload;
      }
      console.log(44, data);
      return {
        // ...state,
        element: data
      };
    }
    case UPDATE_APPLICATION:
      return {
        // ...state,
        ele: action.payload
      };
    case DELETE_APPLICATION:
      // const _id = action.payload._id;
      return {
        ...state,
        app: action.payload
      };
    case FILTER_APPLICATION:
      return {
        ...state,
        versionFilter: action.payload
      };
    case FILTER1_APPLICATION:
      return {
        ...state,
        items: action.payload
      };
    // console.log("Inside Reducer");
    // return state.filter((data, i) => i !== action.id);
    default:
      return state;
  }
}
