import { EDIT_TOOL } from "../actions/types";

const initialState = {
  element: {}
};

export default function reducer(state = initialState, action) {
  console.log(action.type, action.payload);
  switch (action.type) {
    case EDIT_TOOL: {
      let data = [];
      if (action.payload !== undefined) {
        data = action.payload;
      }
      console.log(44, data);
      return {
        // ...state,
        element: data
      };
    }
    default:
      return state;
  }
}
