import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, Select, Card } from "antd";
import { Table, InputNumber, Popconfirm } from "antd";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { fetchUser } from "../redux/actions/userAction";
import { deleteuserStore } from "../redux/actions/projectAction";
const EditableContext = React.createContext();
let data2 = [];

export class EditableCell extends React.Component {
  constructor(props) {
    super(props);
    this.state = { disabled: true };
  }

  getInput = () => {
    if (this.props.inputType === "text") {
      return <Input disabled={this.state.disabled ? "disabled" : ""} />;
    } else if (this.props.inputType === "list") {
      return (
        <Input disabled={this.state.disabled ? "disabled" : ""} />
      );
    }
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    if (editing && title === "User name" && inputType === "text") {
      console.log(
        "dataIndex : ",
        record,
        record.username.UserName,
        dataIndex,
        record.userrole.Role
      );
      return (
        <td {...restProps}>
          <Form.Item style={{ margin: 0 }}>
            <Input
              disabled={this.state.disabled ? "disabled" : ""}
              value={record.username.UserName}
            />
          </Form.Item>
        </td>
      );
    }
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              initialValue: record[dataIndex],
              rules: [
                {
                  message: `Please Input ${title}!`,
                  required: true
                }
              ]
            })(this.getInput())}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return (
      <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
    );
  }
}
class EditableTable extends React.Component {
  componentDidMount() {
    //this.props.fetchUser();
  }

  constructor(props) {
    super(props);
    this.state = { update: false, data2, editingKey: "" };
    this.columns = [
      {
        dataIndex: "username",
        editable: true,
        title: "User name",
        width: "25%"
      },
      {
        dataIndex: "userrole",
        editable: true,
        title: "Role",
        width: "40%"
      },
      {
        dataIndex: "operation",
        render: (text, record) => {
          console.log("Recordddddddddddddddddddddddddddddddddddddddd",record);
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <a
                    onClick={() => this.save(form, record.username)}
                    style={{ marginRight: 8 }}
                  >
                    Save
                  </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm
                title="Sure to cancel?"
                onConfirm={() => this.cancel(record.username)}
              >
                <a>Cancel</a>
              </Popconfirm>
            </span>
          ) : (
            <span>
              <a
                disabled={editingKey !== "" || this.props.editDisabled}
                onClick={() => this.edit(record.username)}
                style={{display:"none"}}
              >
                Edit
              </a>
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.handleDelete(record.username,record.objId)}
              >
                <a disabled={this.props.editDisabled} style={{color:"red"}}> Delete</a>
              </Popconfirm>
            </span>
          );
        },
        title: "Action"
      }
    ];
  }

  isEditing = record => record.username === this.state.editingKey;

  cancel = () => {
    this.setState({ editingKey: "" });
  };

  save(form, username) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }

      const newData = [];
      this.props.data2.forEach(tt => {
        if (tt.username === username.UserName) {
          tt[`userrole`] = "dfhgrfdhfdh";
        }
        newData.push(tt);
      });
      const index = newData.findIndex(item => {
        if (username.UserName === item.username.UserName) {
          return true;
        } else {
          return false;
        }
      });
      if (index > -1) {
        data2 = newData;
      } else {
        newData.push(row);
      }
      this.setState({ data2: newData, editingKey: "" });
    });
  }

  edit(username) {
    this.setState({ editingKey: username });
  }

  handleDelete = (username, userId) => {
    this.props.userrole.forEach(user => {
      console.log("Userrrrrrrrrrrrrrrrrrrrrrrrrrr",user);
      console.log("check equality=>>>>>>",user.objId+"::::::::::"+userId);
      //console.log(userId,"User id");
      if(user.objId === userId) {
      console.log("handledelete id :::::>>>>>>>",userId);
      this.props.deleteuserStore(userId);
      }
    });
    const newData = [...this.props.userrole];
    const index = newData.findIndex(item => {
      console.log("itemmmmmmmmmm", item);
      if (username === item.username) {
        return true;
      } else {
        return false;
      }

    });
    if (index > -1) {
      this.props.userrole.splice(index, 1);
       this.setState({
        userrole: newData.filter(item => item.username !== username)
      });
    }
    console.log("this.props.userrole>>>>>>>>>>>>",this.props.userrole);
  };
  render() {
    const components = {
      body: {
        cell: EditableCell
      }
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          dataIndex: col.dataIndex,
          editing: this.isEditing(record),
          inputType: col.title === "User name" ? "text" : "list",
          record,
          title: col.title
        })
      };
    });

    return (
      <EditableContext.Provider value={this.props.form}>
        <Table
          components={components}
          bordered
          //dataSource={this.loadData()}
           dataSource={this.props.userrole}
          //dataSource={this.props.data2}
          columns={columns}
          rowClassName="editable-row"
          pagination={{
            onChange: this.cancel
          }}
        />
      </EditableContext.Provider>
    );
  }
}
EditableTable.propTypes = {
  element: PropTypes.object.isRequired,
  fetchUser: PropTypes.func.isRequired,
  user: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  data2: state.editproject.data2,
  element: state.editproject.element,
  role: state.role.datas2,
  //user: state.editproject.element.user,
  user: state.user.datas,
  userrole: state.project.userrole
});

const EditableFormTable = Form.create()(EditableTable);
export default connect(
  mapStateToProps,
  { fetchUser, deleteuserStore }
)(EditableFormTable);
