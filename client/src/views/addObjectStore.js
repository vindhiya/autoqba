import React,{ Component } from 'react';
import { Form, Input, Button, Select, Card, Breadcrumb, Row } from "antd";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import DynamicFields from "./dynamicFields";
import "antd/dist/antd.css";
const { TextArea } = Input;
const { Option } = Select;
import {createRepositoryStore} from "../redux/actions/repositoryStoreAction";
 class AddObjectStore extends React.Component {

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
            console.log(JSON.stringify(values));
            console.log("Received values of form: ", values);
           this.props.createRepositoryStore(values);
          }
        });
      };
      
    render(){

        const { getFieldDecorator } = this.props.form;
        const { form } = this.props;
        return (
            <Form onSubmit={this.handleSubmit}>
            <div className="container-fluid">
              <div className="Breadcrumb-top">
                <Row>
                  <Breadcrumb>
                    <Breadcrumb.Item>
                      {" "}
                      <Button type="link" onClick={this.redirecthome}>
                        {" "}
                        Home{" "}
                      </Button>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                      <Button type="link" onClick={this.redirecthome}>
                        {" "}
                        Object Store{" "}
                      </Button>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>New Object Store</Breadcrumb.Item>
                  </Breadcrumb>
                </Row>
              </div>
    
              <Row className="margintop">
                <div className="float-left">
                  <h5>New Store</h5>
                </div>
                <div className="float-right">
                  <Button type="primary" htmlType="submit">
                    Create Object Store
                  </Button>
                </div>
                <hr className="hrline"></hr>
              </Row>
    
              <div className="cardheader">
                <Card className="antcardborder">
                  <div className="row">
                  
                  <div className="col-md-6 col-sm-12">
                      <Form.Item label="Select Class & Name">
                        <DynamicFields
                          {...form}
                          name="object_type"
                          fields={[
                            {
                              field: () => (
                                <Input />
                              ),
                              name: "class"
                            },
                            {
                              field: () => (
                                <Input />
                              ),
                              name: "name"
                            }
                          ]}
                        />
                      </Form.Item>
                    </div>
                                     
                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Locator Name">
                        {getFieldDecorator("locator", {
                          rules: [
                            {
                              message: "Please enter Locator name",
                              required: true
                            },
                            {
                              max: 20,
                              message: "name should be accept maximum 10 characters"
                            },
                            {
                              message: "name should be minimum 3 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Locator Value">
                        {getFieldDecorator("locator_value", {
                          rules: [
                            {
                              message: "Please enter Locator Value",
                              required: true
                            },
                            {
                              max: 20,
                              message: "name should be accept maximum 10 characters"
                            },
                            {
                              message: "name should be minimum 3 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Screen">
                        {getFieldDecorator("screen", {
                          rules: [
                            {
                              message: "Please enter Screen name",
                              required: true
                            },
                            {
                              max: 20,
                              message: "name should be accept maximum 10 characters"
                            },
                            {
                              message: "name should be minimum 3 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Field">
                        {getFieldDecorator("field", {
                          rules: [
                            {
                              message: "Please enter Field name",
                              required: true
                            },
                            {
                              max: 20,
                              message: "name should be accept maximum 10 characters"
                            },
                            {
                              message: "name should be minimum 3 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Status">
                        {getFieldDecorator("status", {
                          rules: [
                            {
                              message: "Please enter Status",
                              required: true
                            },
                            {
                              max: 20,
                              message: "name should be accept maximum 10 characters"
                            },
                            {
                              message: "name should be minimum 3 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>
    
                    
                   
    
                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Visibility">
                        {getFieldDecorator("visibility", {
                          initialValue: "public",
                          rules: [
                            {
                              message: "Please input your Role!",
                              required: true
                            }
                          ]
                        })(
                          <Select>
                            <Option value="public">Public</Option>
                            <Option value="private">Private</Option>
                          </Select>
                        )}
                      </Form.Item>
                    </div>
                  </div>
                </Card>
              </div>
            </div>
          </Form>
        );
    };
}

AddObjectStore.propTypes = {
    createRepositoryStore: PropTypes.func.isRequired,
    
  };
  
  const mapStateToProps = state => ({
    objectItem: state.repositoryStore.objectItem
  });

const AddObjectStoreForm = Form.create({ name: "register" })(AddObjectStore);


export default connect(
    mapStateToProps,
    { createRepositoryStore }
  )(AddObjectStoreForm);
  