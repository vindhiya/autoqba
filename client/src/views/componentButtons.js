import React, { Component } from "react";
import { Button } from "antd";

class ComponentButtons extends Component {
  constructor(props) {
    super(props);
    this.state = { isReadOnly: true };
  }

  handleEdit = e => {
    e.preventDefault();
    this.setState({ isReadOnly: false });
    this.props.handleEdit();
  };

  handleCancel = e => {
    e.preventDefault();
    this.setState({ isReadOnly: true });
    this.props.handleCancel();
  };

  render() {
    return (
      <div className="float-right">
        {this.state.isReadOnly ? (
          <div className="float-right">
            <Button type="primary" htmlType="submit" onClick={this.handleEdit}>
              Edit
            </Button>&nbsp;&nbsp;&nbsp;&nbsp;
            <Button
              onClick={this.props.handleDelete}
              className="btn btn-danger"
              style={{ float: "right" }}
            >
              Delete
            </Button>
          </div>
        ) : (
          <div className="float-left">
            <Button
              type="primary"
              htmlType="submit"
              onClick={this.props.handleUpdate}
            >
              Update
            </Button>&nbsp;&nbsp;&nbsp;&nbsp;
            <Button
              style={{ float: "right" }}
              onClick={this.handleCancel}
              className="btn btn-warning"
            >
              Cancel
            </Button>
          </div>
        )}
      </div>
    );
  }
}

export default ComponentButtons;
