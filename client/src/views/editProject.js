import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, Select, Card, Breadcrumb, Row } from "antd";
import {
  updateProject,
  deleteProject,
  userupdateProject,
  inmemoryAddUser,
  filterUser,
  updateProjectstatus
} from "../redux/actions/projectAction";
import { connect } from "react-redux";
import { fetchUser } from "../redux/actions/userAction";
import PropTypes from "prop-types";
import DynamicFields from "./dynamicFields";
import EditableFormTable from "./projectUserTable";
import CommonButtons from "./commonButton";
import { fetchRoleWithParam } from "../redux/actions/roleAction";

const { TextArea } = Input;
const { Option } = Select;

export class EditProject extends React.Component {
  componentWillMount() {
    this.props.fetchUser();
    this.props.fetchRoleWithParam("Project");
    const { match, location, history } = this.props;
    let myLoc = JSON.stringify(location.pathname);
    var finalstr = myLoc.replace(/"/g, "");
    this.editedComponent = finalstr.substring(finalstr.lastIndexOf("/") + 1);
    console.log("filter component store : " + this.editedComponent);
    this.props.filterUser(this.editedComponent);
  }

  constructor(props) {
    super(props);
    this.state = { disabled: true, modifyproject: "View Project" };
  }

  handleEdit = e => {
    this.setState({ disabled: false });
    this.setState({ modifyproject: "Edit Project" });
  };

  handleCancel = e => {
    this.setState({ disabled: true });
    this.setState({ modifyproject: "View Project" });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      values[`updated_by`] = this.props.uid;
      if (!err) {
        const users = [];
        console.log("users : ", this.props.userrole);
        this.props.userrole.forEach(userrole =>{
          let u = userrole;
          this.props.user.forEach(user => {
            if(u.username === user.UserName) {
              u.username = user.userid;
            }
          })
          this.props.role.forEach(role => {
            if(u.userrole === role.Role) {
              u.userrole = role.roleid;
            }
          })
          users.push(u);
          console.log("kamssssssssssssss",users)
        })
        values.user = users;
        console.log("Users",values.user);
        console.log("Received values of form: ", values);
        this.props.updateProject(this.props.element._id, values);
        this.props.history.replace("/viewproject/");
      } else {
        alert("Enter correct details..");
      }
    });
  };

  handleuserUpdate = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let usrArr = [
          {
            username: values.username,
            userrole: values.userrole
          }
        ];

        let oldArr = this.props.userrole;
        console.log("OldArrayyyyyyyyyyyyy",oldArr);
        var k = 0;
        for (let i = 0; i < oldArr.length; i++) {
          if (usrArr[0].username === oldArr[i].username) {
            k++;
          }
        }
        if (k === 0) {
          usrArr = usrArr.concat(oldArr);
          console.log("Ippo panna array", usrArr);
          values.user = usrArr;
          console.log("Element id",this.props.element._id);
          this.props.inmemoryAddUser(this.props.element._id, values);
        } else {
          alert("user already exist");
        }
        this.props.form.setFields({
          username: undefined,
          userrole: undefined
        });
      }
    });
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  handleDelete = e => {
   // e.preventDefault();
    console.log(1, "Inside method --> handleDelete");
    this.props.deleteProject(this.props.element._id);
    //alert("Project Deleted..");
    this.props.history.push("/viewproject/");
  };

  handleDeactivate = e => {
   // e.preventDefault();
   // alert("Deactivated");
    this.props.updateProjectstatus(this.props.element._id);
    this.props.history.push("/viewproject/");
  };

  handleActivate = e => {
   // e.preventDefault();
   // alert("Activated");
    this.props.updateProjectstatus(this.props.element._id);
    this.props.history.push("/viewproject/");
  };

  handleClone = e => {
   // e.preventDefault();
  //  alert("Cloned");
    this.props.history.push("/viewproject/");
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { form } = this.props;
    console.log("This props",this.props.element);
    return (
      <Form>
        <div className="container-fluid">
          <div className="container-fluid">
            <div className="Breadcrumb-top">
              <Row>
                <Breadcrumb>
                  <Breadcrumb.Item>
                    {" "}
                    <Button type="link" onClick={this.redirecthome}>
                      {" "}
                      Home{" "}
                    </Button>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Project</Breadcrumb.Item>
                  <Breadcrumb.Item>{this.state.modifyproject}</Breadcrumb.Item>
                </Breadcrumb>
              </Row>
            </div>

            <Row className="margintop">
              <div className="float-left">
                <h5>{this.state.modifyproject}</h5>
              </div>
              <CommonButtons
                handleEdit={this.handleEdit}
                handleDelete={this.handleDelete}
                handleUpdate={this.handleSubmit}
                handleCancel={this.handleCancel}
                handleDeactivate={this.handleDeactivate}
                handleActivate={this.handleActivate}
                handleClone={this.handleClone}
                status={this.props.element.status}
              ></CommonButtons>
              <hr className="hrline"></hr>
            </Row>

            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Name">
                    {getFieldDecorator("project_name", {
                      initialValue: this.props.element.project_name,
                      rules: [
                        {
                          message: "Please enter name",
                          required: true
                        },
                        {
                          max: 100,
                          message: "name should be accept maximum 20 characters"
                        },
                        {
                          message: "name should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Project Description">
                    {getFieldDecorator("project_desc", {
                      initialValue: this.props.element.project_desc,
                      rules: [
                        {
                          message: "Please enter Description",
                          required: true
                        },
                        {
                          max: 1000,
                          message:
                            "Project Description should be accept maximum 1000 characters"
                        },
                        {
                          message:
                            "Project Description should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(
                      <TextArea
                        autosize={{ minRows: 2, maxRows: 3 }}
                        disabled={this.state.disabled ? "disabled" : ""}
                      />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("visibility", {
                      initialValue: this.props.element.visibility,
                      //initialValue: "public",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>

              <Row>
                <h5>Select Users & Roles</h5>
              </Row>
              <div className="row">
                <div className="col-md-5 col-sm-12">
                  <Form.Item label="Select Username">
                    {getFieldDecorator("username", {
                      rules: [
                        {
                          message: "Please select username",
                          required: false
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select username"
                        disabled={this.state.disabled ? "disabled" : ""}
                      >
                        {this.props.user.map((obj, index) => (
                          <Option key={obj.UserName} value={obj.UserName }>
                            {obj.UserName}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-5 col-sm-12">
                  <Form.Item label="Select Role">
                    {getFieldDecorator("userrole", {
                      rules: [
                        {
                          message: "Please select Role",
                          required: false
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select userrole"
                        disabled={this.state.disabled ? "disabled" : ""}
                      >
                        {this.props.role.map((obj, index) => (
                          <Option key={obj.Role} value={obj.Role}>
                            {obj.Role}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-2 col-sm-12">
                  <Form.Item>&nbsp;</Form.Item>
                  <Form.Item>
                    <Button
                      type="primary"
                      shape="circle"
                      icon="plus"
                      onClick={this.handleuserUpdate}
                    ></Button>
                  </Form.Item>
                </div>
              </div>

              {/* <Form.Item>
                <Button
                  type="primary"
                  onClick={this.handleUpdate}
                  disabled={this.state.disabled ? "disabled" : ""}
                >
                  {" "}
                  Add user
                </Button> */}

              <Form.Item>
                <Row className="margintop">
                  <div className="float-left">
                    <h5>Existing Users</h5>
                  </div>
                </Row>
                <EditableFormTable
                  editDisabled={this.state.disabled}
                ></EditableFormTable>
              </Form.Item>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}
EditProject.propTypes = {
  element: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  data2: state.editproject.data2,
  element: state.editproject.element,
  role: state.role.datas2,
  uid: state.loginData.userid,
  uname: state.loginData.userName,
  user: state.user.datas,
  userrole: state.project.userrole
});

const WrappedEditProject = Form.create({ name: "register" })(EditProject);

export default connect(
  mapStateToProps,
  {
    deleteProject,
    fetchRoleWithParam,
    fetchUser,
    filterUser,
    inmemoryAddUser,
    updateProject,
    updateProjectstatus,
    userupdateProject
  }
)(WrappedEditProject);
