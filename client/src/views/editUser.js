import React, { Component } from "react";
import "antd/dist/antd.css";
import { Table } from "antd";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  updateUser,
  deleteUser,
  updateUserstatus,
  getUserProject
} from "../redux/actions/userAction";
import { fetchProject } from "../redux/actions/projectAction";
import { fetchRole, fetchRoleWithParam } from "../redux/actions/roleAction";
import {
  Form,
  Input,
  Button,
  Switch,
  Select,
  Card,
  Breadcrumb,
  Row,
  Alert,
  Icon
} from "antd";
import DynamicFields from "./dynamicFields";
const { TextArea } = Input;
const { Option } = Select;
import Highlighter from "react-highlight-words";
import ComponentButtons from "./componentButtons";
import EditableFormTable from "./userProjectAssociation";
import Global from "../components/layout/Global/Global";
import CommonButtons from "./commonButton";

export class EditUser extends Component {
  componentDidMount() {
    this.props.fetchRole();
    this.props.fetchProject();
    this.props.fetchRoleWithParam("System");
    console.log("edit element :::",this.props.element);
  }

  constructor(props) {
    super(props);
    this.state = { disabled: true, modifyuser: "View User" };
  }

  handleEdit = e => {
    this.setState({ disabled: !this.state.disabled });
    this.setState({ modifyuser: "Edit User" });
  };

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={String(text)}
        //textToHighlight={text.toString()}
      />
    )
  });

  handleCancel = e => {
    this.setState({ disabled: true });
    this.setState({ modifyuser: "View User" });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      values[`userid`] = this.props.userid;
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.updateUser(this.props.element._id, values);
        alert("User Updated..");
        console.log("User Updated 111");
        this.props.history.push("/viewuser/");
      } else {
        alert("Enter correct details..");
      }
    });
  };

  handleDelete = e => {
   // e.preventDefault();
    console.log(1, "Inside method --> handleDelete");
    this.props.deleteUser(this.props.element._id);
    //alert("User Deleted..");
    this.props.history.push("/viewuser/");
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  redirectview = e => {
    e.preventDefault();
    this.props.history.push("/viewuser");
  };

  handleDeactivate = e => {
   // e.preventDefault();
   // alert("Deactivated");
    this.props.updateUserstatus(this.props.element._id);
    this.props.history.push("/viewuser");
  };

  handleActivate = e => {
   // e.preventDefault();
   // alert("Activated");
    this.props.updateUserstatus(this.props.element._id);
    this.props.history.push("/viewuser");
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { form } = this.props;
    //const userrole= this.props.user.userrole;
    const data = [];
    {
      // if(this.props.items){
      //   this.props.items.forEach(object=>{
      //     console.log("Userid",object)
      //     this.props.user.filter(role =>{
      //       console.log("userrole",role.userrole)
      //     });
      //     data.push({
      //       project_name:object.project_name,
      //       userrole:object.userrole
      //     })
      //   })
      // }
      this.props.items.map(object =>
        data.push({
          project_name: object.project_name,
          project_userrole: object.project_userrole
        })
      );
    }

    const columns = [
      {
        dataIndex: "project_name",
        key: "project_name",
        title: "Project Name",
        ...this.getColumnSearchProps("project_name")
      },
      {
        dataIndex: "project_userrole",
        key: "project_userrole",
        title: "Role",
        ...this.getColumnSearchProps("project_userrole")
      }
    ];
    //console.log(112,this.props.project);
    //const project = this.props.project;
    //console.log(project);
    return (
      <Form>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>User</Breadcrumb.Item>
                <Breadcrumb.Item>{this.state.modifyuser}</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>{this.state.modifyuser}</h5>
            </div>

            <CommonButtons
              handleEdit={this.handleEdit}
              handleDelete={this.handleDelete}
              handleUpdate={this.handleSubmit}
              handleCancel={this.handleCancel}
              handleDeactivate={this.handleDeactivate}
              handleActivate={this.handleActivate}
              status={this.props.element.Status}
            ></CommonButtons>

            <hr className="hrline"></hr>
          </Row>

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="First Name">
                    {getFieldDecorator("FirstName", {
                      initialValue: this.props.element.FirstName,
                      rules: [
                        {
                          message: "Please enter FirstName",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "FirstName should be accept maximum 20 characters"
                        },
                        {
                          message: "FirstName should be minimum 4 characters",
                          min: 4
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Last Name">
                    {getFieldDecorator("LastName", {
                      initialValue: this.props.element.LastName,
                      rules: [
                        {
                          message: "Please enter LastName",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "LastName should be accept maximum 20 characters"
                        },
                        {
                          message: "LastName should be minimum 2 characters",
                          min: 4
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="User Name">
                    {getFieldDecorator("UserName", {
                      initialValue: this.props.element.UserName,
                      rules: [
                        {
                          message: "Please enter UserName",
                          required: true
                        },
                        {
                          max: 15,
                          message:
                            "UserName should be accept maximum 20 characters"
                        },
                        {
                          message: "UserName should be minimum 4 characters",
                          min: 8
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Email">
                    {getFieldDecorator("Email", {
                      initialValue: this.props.element.Email,
                      rules: [
                        {
                          message: "Please enter Email",
                          required: true
                        },
                        {
                          max: 35,
                          message:
                            "Email should be accept maximum 20 characters"
                        },
                        {
                          message: "Email should be minimum 4 characters",
                          min: 4
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Company Name">
                    {getFieldDecorator("CompanyName", {
                      initialValue: this.props.element.CompanyName,
                      rules: [
                        {
                          message: "Please enter CompanyName",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "CompanyName should be accept maximum 20 characters"
                        },
                        {
                          message: "CompanyName should be minimum 4 characters",
                          min: 3
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Role">
                    {getFieldDecorator("systemRole", {
                      initialValue: this.props.element.systemRole,
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        disabled={this.state.disabled ? "disabled" : ""}
                        layout="inline"
                        showSearch
                        placeholder="Select Role"
                      >
                        {this.props.systemRole.map(obj => (
                          <Option key={obj.Role} value={obj.Role}>
                            {obj.Role}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("Visibility", {
                      initialValue: this.props.element.Visibility,
                      rules: [
                        {
                          message: "Please input your Visibility!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Public">Public</Option>
                        <Option value="Private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Status">
                    {getFieldDecorator("Status", {
                      initialValue: this.props.element.Status,
                      rules: [
                        {
                          message: "Please input your Status!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Active">Active</Option>
                        <Option value="Deactive">Deactive</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>

              
              <Row>
                <h5>Select Project & Role</h5>
              </Row>
              <div className="row">
                <div className="col-md-5 col-sm-12">
                  <Form.Item label="Select Project name">
                    {getFieldDecorator("projectname", {
                      rules: [
                        {
                          message: "Please select project name",
                          required: false
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select projectname"
                      >
                        {this.props.project.map((obj, index) => (
                          <Option key={obj.project_name} value={index}>
                            {obj.project_name}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-5 col-sm-12">
                  <Form.Item label="Select Role">
                    {getFieldDecorator("Role", {
                      rules: [
                        {
                          message: "Please select Role",
                          required: false
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select Role"
                      >
                        {this.props.projectRole.map((obj, index) => (
                          <Option key={obj.Role} value={index}>
                            {obj.Role}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-2 col-sm-12">
                  <Form.Item>&nbsp;</Form.Item>
                  <Form.Item>
                    <Button
                      type="primary"
                      shape="circle"
                      icon="plus"
                      onClick={this.handleProjectUpdate}
                      //disabled={this.isValid()}
                    ></Button>
                  </Form.Item>
                </div>
              </div>


              <div className="cardheader">
                <Card>
                  <Table
                    dataSource={data}
                    columns={columns}
                    pagination={{ pageSize: 28 }}
                    rowKey="_id"
                  />
                  <div style={{ color: "red" }} />
                </Card>
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}
EditUser.propTypes = {
  deleteUser: PropTypes.func.isRequired,
  element: PropTypes.object.isRequired,
  fetchProject: PropTypes.func.isRequired,
  fetchRole: PropTypes.func.isRequired,
  fetchRoleWithParam: PropTypes.func.isRequired,
  project: PropTypes.array.isRequired,
  projectRole: PropTypes.object.isRequired,
  role: PropTypes.array.isRequired,
  updateUser: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  element: state.edituser.element,
  items: state.project.items,
  project: state.project.items,
  projectRole: state.role.datas2,
  role: state.role.datas,
  systemRole: state.role.datas1,
  userid: state.loginData.userid
});
const EditUserForm = Form.create({ name: "register" })(EditUser);
export default connect(
  mapStateToProps,
  {
    deleteUser,
    fetchProject,
    fetchRole,
    fetchRoleWithParam,
    getUserProject,
    updateUser,
    updateUserstatus
  }
)(EditUserForm);
