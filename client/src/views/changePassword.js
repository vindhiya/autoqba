import React from "react";
import "antd/dist/antd.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Form, Input, Button, Switch, Select, Card,Breadcrumb,Row,Alert,Col } from "antd";
import DynamicFields from "./dynamicFields";
import Global from '../components/layout/Global/Global'
import { changePassword } from '../redux/actions/userAction';
import FormItem from 'antd/lib/form/FormItem';

const { TextArea } = Input;
const { Option } = Select;

class Changepassword extends React.Component {
    state = {
        confirmDirty: false
      };
    
      handleSubmit = e => {
        e.preventDefault();
        console.log("52525",this.props.email);
        this.props.form.validateFieldsAndScroll((err, values) => {
           if (!err) {
                this.props.changePassword(values);
             }
            });
        };

    compareToFirstPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue('NewPassword')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    };

    validateToNextPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && this.state.confirmDirty) {
            form.validateFields(['ConfirmPassword'], { force: true });
        }
        callback();
    };
    setEmail = (callback) => {
      const { form } = this.props.email;
      
      callback();
  };
    componentWillMount(){
        Global.clearAlert();
      }
    handleClose(){
        Global.history.push("/home");
       }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { form } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Change Password</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop" >
              <h4>Change Password ?</h4>
         
          <hr className="hrline"></hr>
          </Row>

          <Row type="flex" justify="center">
           <Col span={8}>
                {
                        Global.alertContent.message?
                        <Alert
                          message={Global.alertContent.status.toUpperCase()}
                          description={Global.alertContent.message}
                          type={Global.alertContent.status}
                          closable={Global.alertContent.closable}
                          closeText="OK"
                          afterClose={this.handleClose}
                          showIcon
                        />:null
                }
          </Col>
         </Row>
          <Row>
              <div className="cardheader">
                <Card className="antcardborder">
                  <div className="row">
                    <div className="col-md-6 col-sm-12">
                      
                      <Form.Item label="Old Password">
                        {getFieldDecorator("OldPassword", {
                          rules: [
                            {
                              message: "Please enter OldPassword",
                              required: true
                            },
                            {
                              max: 20,
                              message: "OldPassword should be accept maximum 20 characters"
                            },
                            {
                              message: "OldPassword should be minimum 8 characters",
                              min: 8
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>
                    
                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="New Password" hasFeedback>
                        {getFieldDecorator("NewPassword", {
                          rules: [
                            {
                              message: "Please enter NewPassword",
                              required: true
                            },
                            {
                              max: 20,
                              message:
                                "NewPassword should be accept maximum 20 characters"
                            },
                            {
                              message:
                                "NewPassword should be minimum 8 characters",
                              min: 8
                            },
                            {
                                validator: this.validateToNextPassword,
                            },
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Confirm Password" hasFeedback>
                        {getFieldDecorator("ConfirmPassword", {
                          rules: [
                            {
                              message: "Please enter ConfirmPassword",
                              required: true
                            },
                            {
                              max: 20,
                              message:
                                "ConfirmPassword should be accept maximum 20 characters"
                            },
                            {
                              message: "ConfirmPassword should be minimum 8 characters",
                              min: 8
                            },
                            {
                                validator: this.compareToFirstPassword
                            },
                          ]
                        })(<Input />)}
                      </Form.Item>
                      </div>
                      <div>
                      <Form.Item>
                        {getFieldDecorator("Email", {
                            initialValue: this.props.email
                        })(<Input hidden/>)}
                      </Form.Item>
                      

                      </div>
                      </div>
                 <Form.Item>
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
              </Form.Item>
                </Card>
              </div>
          </Row>
          
        </div>
      </Form>
    );
  }
}

Changepassword.propTypes = {
    changePassword: PropTypes.func.isRequired,
};


const mapStateToProps = state => ({
    email: state.loginData.pass.Email,
    pass: state.user.pass
    
});

const Createpasswordform = Form.create({ name: "register" })(
    Changepassword
);

export default connect(
    mapStateToProps,
  { changePassword }
)(Createpasswordform);
