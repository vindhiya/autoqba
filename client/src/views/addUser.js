import React from "react";
import "antd/dist/antd.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  createUser,
  userprojectUpdate,
  emptyprojectupdate
} from "../redux/actions/userAction";
import { fetchRoleWithParam } from "../redux/actions/roleAction";
import { fetchProject } from "../redux/actions/projectAction";
import {
  Form,
  Input,
  Button,
  Switch,
  Select,
  Card,
  Breadcrumb,
  Row,
  Alert,
  Col
} from "antd";
import DynamicFields from "./dynamicFields";
import EditableFormTable from "./addUserTable";
import Global from "../components/layout/Global/Global";
const { TextArea } = Input;
const { Option } = Select;

class Adduser extends React.Component {
  componentWillMount() {
    console.log("Projects", this.props.project);
    this.props.emptyprojectupdate();
    this.props.fetchProject();
    this.props.fetchRoleWithParam("System");
    this.props.fetchRoleWithParam("Project");
    console.log("Projects", this.props.project);
  }

  constructor() {
    super();
    this.state = {
      childVisible: false
    };
  }
  // isValid() {
  //   console.log("val : ", this.state.projectname);
  //   console.log("val : ", this.state.Role);
  //   let val = false;
  //   if (this.state.projectname && this.state.Role) {
  //     val = true;
  //   }
  //   console.log("val : ", this.state.projectname);
  //   return val;
  // }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const projects = [];
        // console.log("users : ", users);
        this.props.data3.forEach(project => {
          let u = project;
          //let v=projectRole;
          u.projectname = project.projectname.project_id;
          console.log(100, u.projectname);
          console.log(101, project.Role.roleid);
          u.Role = project.Role.roleid;
          console.log(101, u.Role);

          projects.push(u);
          console.log(121, projects);
        });

        values.project = projects;
        values[`project_id`] = this.props.project_id;
        // values.projectRole=projects;
        // values[`roleid`]=this.props.roleid;

        console.log(JSON.stringify(values));
        console.log("Received values of form: ", values);
        this.props.createUser(values);
        console.log("userrrrrr", this.props.data3);
      } else {
        alert("Enter correct details..");
      }
    });
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };
  // componentWillMount(){
  //   Global.clearAlert();
  // }

  handleClose() {
    //this.props.history.push("/viewuser/");
    Global.history.push("/viewuser");
  }

  handleProjectUpdate = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(100, values);
        this.setState({ childVisible: true });

        let userArr = [
          {
            Role: this.props.projectRole[values.Role],
            projectname: this.props.project[values.projectname]
          }
        ];
        console.log("userArr : ", userArr);

        let oldArr = this.props.data3;
        var k = 0;
        for (let i = 0; i < oldArr.length; i++) {
          if (userArr[0].projectname === oldArr[i].projectname) {
            k++;
          }
        }
        if (k === 0) {
          userArr = userArr.concat(oldArr);
          console.log(101, userArr);
          values.project = userArr;
          this.props.userprojectUpdate(values);
        }
        // else {
        //   alert("pro already exist");
        // }
        this.props.form.setFields({
          Role: undefined,
          projectname: undefined
        });
      }

      // var nproject = values.project;
      // const project = values.project.concat(this.props.data3);
      // values.project = project;

      //   this.props.userprojectUpdate(values);
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { form } = this.props;
    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>User</Breadcrumb.Item>
                <Breadcrumb.Item>New User</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>New User</h5>
            </div>
            <div className="float-right">
              <Button type="primary" htmlType="submit">
                Create User
              </Button>
            </div>
            <hr className="hrline"></hr>
          </Row>

          <Row type="flex" justify="center">
            <Col span={8}>
              {Global.alertContent.message ? (
                <Alert
                  message={Global.alertContent.status.toUpperCase()}
                  description={Global.alertContent.message}
                  type={Global.alertContent.status}
                  closable={Global.alertContent.closable}
                  closeText="OK"
                  afterClose={this.handleClose}
                  showIcon
                />
              ) : null}
            </Col>
          </Row>

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="First Name">
                    {getFieldDecorator("FirstName", {
                      rules: [
                        {
                          message: "Please enter FirstName",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "FirstName should be accept maximum 20 characters"
                        },
                        {
                          message: "FirstName should be minimum 4 characters",
                          min: 4
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Last Name">
                    {getFieldDecorator("LastName", {
                      rules: [
                        {
                          message: "Please enter LastName",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "LastName should be accept maximum 20 characters"
                        },
                        {
                          message: "LastName should be minimum 2 characters",
                          min: 4
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="User Name">
                    {getFieldDecorator("UserName", {
                      rules: [
                        {
                          message: "Please enter UserName",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "UserName should be accept maximum 20 characters"
                        },
                        {
                          message: "UserName should be minimum 8 characters",
                          min: 8
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Email">
                    {getFieldDecorator("Email", {
                      rules: [
                        {
                          message: "Please enter valid Email",
                          required: true,
                          type: "email"
                        },
                        {
                          max: 35,
                          message:
                            "Email should be accept maximum 35 characters"
                        },
                        {
                          message: "Email should be minimum 4 characters",
                          min: 4
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Company Name">
                    {getFieldDecorator("CompanyName", {
                      rules: [
                        {
                          message: "Please enter CompanyName",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "CompanyName should be accept maximum 20 characters"
                        },
                        {
                          message: "CompanyName should be minimum 4 characters",
                          min: 3
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Role">
                    {getFieldDecorator("systemRole", {
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select Role"
                      >
                        {this.props.systemRole.map(obj => (
                          <Option key={obj.Role} value={obj.Role}>
                            {obj.Role}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("Visibility", {
                      initialValue: "Public",
                      rules: [
                        {
                          message: "Please input your Visibility!",
                          required: true
                        }
                      ]
                    })(
                      <Select>
                        <Option value="Public">Public</Option>
                        <Option value="Private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Status">
                    {getFieldDecorator("Status", {
                      initialValue: "Active",
                      rules: [
                        {
                          message: "Please input your Status!",
                          required: true
                        }
                      ]
                    })(
                      <Select>
                        <Option value="Active">Active</Option>
                        <Option value="Deactive">Deactive</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>

              <Row>
                <h5>Select Project & Role</h5>
              </Row>
              <div className="row">
                <div className="col-md-5 col-sm-12">
                  <Form.Item label="Select Project name">
                    {getFieldDecorator("projectname", {
                      rules: [
                        {
                          message: "Please select project name",
                          required: false
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select projectname"
                      >
                        {this.props.project.map((obj, index) => (
                          <Option key={obj.project_name} value={index}>
                            {obj.project_name}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-5 col-sm-12">
                  <Form.Item label="Select Role">
                    {getFieldDecorator("Role", {
                      rules: [
                        {
                          message: "Please select Role",
                          required: false
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select Role"
                      >
                        {this.props.projectRole.map((obj, index) => (
                          <Option key={obj.Role} value={index}>
                            {obj.Role}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-2 col-sm-12">
                  <Form.Item>&nbsp;</Form.Item>
                  <Form.Item>
                    <Button
                      type="primary"
                      shape="circle"
                      icon="plus"
                      onClick={this.handleProjectUpdate}
                      //disabled={this.isValid()}
                    ></Button>
                  </Form.Item>
                </div>
              </div>

              {/* <Form.Item>
                    <Button type="primary" 
                    onClick={this.handleProjectUpdate}
                    disabled={this.isValid()}
                    >
                      {" "}
                      Add project
                    </Button>
                  </Form.Item> */}
              <Form.Item>
                {this.state.childVisible ? <EditableFormTable /> : null}
              </Form.Item>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

Adduser.propTypes = {
  createUser: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  fetchProject: PropTypes.func.isRequired,
  fetchRoleWithParam: PropTypes.func.isRequired,
  project: PropTypes.object.isRequired,
  projectRole: PropTypes.object.isRequired,
  role: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  data: state.user.data,
  data3: state.edituser.data3,
  project: state.project.items,
  projectRole: state.role.datas2,
  systemRole: state.role.datas1
});

const Createuserform = Form.create({ name: "register" })(Adduser);

export default connect(
  mapStateToProps,
  {
    createUser,
    emptyprojectupdate,
    fetchProject,
    fetchRoleWithParam,
    userprojectUpdate,
  }
)(Createuserform);
