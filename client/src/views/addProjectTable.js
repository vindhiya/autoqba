import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Select,Row } from "antd";
import { Table, Popconfirm } from "antd";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { fetchUser } from "../redux/actions/userAction";
const { Option } = Select;
const EditableContext = React.createContext();
let data3 = [];
export class EditableCell extends React.Component {
  constructor(props) {
    super(props);
    this.state = { disabled: true };
  }

  getInput = () => {
    if (this.props.inputType === "text") {
      return <Input disabled={this.state.disabled ? "disabled" : ""} />;
    } else if (this.props.inputType === "list") {
      return (
        <Select style={{ width: "100%" }} placeholder="Select Role">
          <Option value="guest">Guest</Option>
          <Option value="developer">Developer</Option>
          <Option value="project admin">Project Admin</Option>
        </Select>
      );
    }
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              initialValue: record[dataIndex],
              rules: [
                {
                  message: `Please Input ${title}!`,
                  required: true
                }
              ]
            })(this.getInput())}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return (
      <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
    );
  }
}
class EditableTable extends React.Component {
  componentDidMount() {
    //this.props.fetchUser();
  }

  constructor(props) {
    super(props);
    this.state = { data3, editingKey: "" };
    this.columns = [
      {
        dataIndex: "username.UserName",
        editable: true,
        title: "User name",
        width: "25%"
      },
      {
        dataIndex: "userrole.Role",
        editable: true,
        title: "Role",
        width: "40%"
      },
      {
        dataIndex: "operation",
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          console.log("record.username : ",record.username)
          return editable? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <a
                    onClick={() => this.save(form, record.username)}
                    style={{ marginRight: 8 }}
                  >
                    Save
                  </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm
                title="Sure to cancel?"
                onConfirm={() => this.cancel(record.username)}
              >
                <a>Cancel</a>
              </Popconfirm>
            </span>
          ) : (
            <span>
              <a
                disabled={editingKey !== ""}
                onClick={() => this.edit(record.username)}
                style={{display:"none"}}
              >
                Edit
              </a>
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.handleDelete(record.username)}
              >
                <a style={{color:"red"}}> Delete</a>
              </Popconfirm>
            </span>
          );
        },
        title: "Action"
      }
    ];
  }

  isEditing = record => record.username === this.state.editingKey;

  cancel = () => {
    this.setState({ editingKey: "" });
  };

  save(form, username) {
    form.validateFields((error, row) => {
      // console.log('row information',row);

      this.props.data3
        .filter(obj => obj.username === row.username)
        .map(
          obj =>
            // console.log('modified obj',row.userrole),
            (obj.userrole = row.userrole)
        );

      const myData = [];
      let len = this.props.data3.length;
      for (let i = 0; i < len; i++) {
        myData.push(this.props.data3[i]);
      }

     // console.log("myDataaaa", myData);

      //  console.log('data22222222',this.props.data3);

      if (error) {
        return;
      }
      /// const newData = [...this.props.data3];
      const newData = myData;
     // console.log("newData : ", newData);
      const index = newData.findIndex(item => {
        if (username === item.username) {
          return true;
        } else {
          return false;
        }
      });

      if (index > -1) {
        // const item = newData[index];
       // console.log("newData : ", newData);
        let list = this.props.data3.splice(index, 1, {
          //...newData,
          ...row
        });
       // console.log("list : ", list);
        data3 = newData;
        this.setState({ data3: newData, editingKey: "" });
       // console.log("Data22222", data3);
      } else {
        newData.push(row);
        this.setState({ data3: newData, editingKey: "" });
      }
    });
  }

  edit(username) {
    this.setState({ editingKey: username });
  }

  handleDelete = username => {
    const newData = [...this.props.data3];
    const index = newData.findIndex(item => {
      console.log("itemmmmmmmmmm", item);
      if (username === item.username) {
        return true;
      } else {
        return false;
      }
    });
    if (index > -1) {
      // const item = newData[index];
      // console.log("Index", index);
      // console.log("newData : ", newData);
      //this.props.data3.pop()
      this.props.data3.splice(index, 1);

      // data3 = newData;
     // console.log("Data22222", data3);

      this.setState({
        data3: newData.filter(item => item.username !== username)
      });
    }
  };

  // handleAdd = () => {
  //   console.log(12345677, this.props.userlist);
  //   var user = {
  //     username: "",
  //     userrole: "",
  //   };

  //   this.props.data3.push(user);
  //   this.setState(this.props.data3);
  //   console.log(12345677, "outside Handle add");
  // };

  render() {
    const components = {
      body: {
        cell: EditableCell
      }
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          dataIndex: col.dataIndex,
          editing: this.isEditing(record),
          inputType: col.title === "User name" ? "text" : "list",
          record,
          title: col.title
        })
      };
    });

    return (
      <EditableContext.Provider value={this.props.form}>
        <Row className="margintop">
                  <div className="float-left">
                    <h5>User List</h5>
                  </div>
                </Row>
        <Table
          components={components}
          bordered
          dataSource={this.props.data3}
          columns={columns}
          rowClassName="editable-row"
          pagination={{
            onChange: this.cancel
          }}
        />
      </EditableContext.Provider>
    );
  }
}
EditableTable.propTypes = {
  element: PropTypes.object.isRequired,
  fetchUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  data3: state.editproject.data3,
  element: state.editproject.element,
  //user: state.editproject.element.user,
  user: state.user.datas
});

const EditableFormTable = Form.create()(EditableTable);
export default connect(
  mapStateToProps,
  { fetchUser }
)(EditableFormTable);

