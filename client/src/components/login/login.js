import React from "react";
import './login-component.css';
import logo from '../../assets/images/logo.jpg';
import { BrowserRouter as Router, Route, Redirect} from 'react-router-dom';
import { Row, Col, Card, Input, Form, Icon, Checkbox, Button, Layout,Alert } from 'antd';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { loginAction, logoutAction } from '../../redux/actions/loginAction';

import Img from 'react-image'
import Global from '../layout/Global/Global';
 
const {Footer}=Layout;

class Login extends React.Component {

  componentWillMount() {
    this.props.logoutAction();
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
            console.log('Received values of form: ', values);
            console.log('in loginnnnnn');
            this.props.loginAction(values);
            console.log(values);
        }
    });
};

clickforgot = e => {
  e.preventDefault();
  Global.history.push("/forgot");
  //this.props.history.push("/forgot");
}

renderRedirect = () => {
  if (this.props.loginData.redirect) {
      return <Redirect to='/home'/>
  }
}

render() {

    const { getFieldDecorator } = this.props.form;

    return(
      <div>        
        <section className="auth-wrapper">
          <div className="login-content">
            <div className="content-wrapper">
              <h1 className="login-content-title">Login to Your Account</h1>
              <Row>
                {this.renderRedirect()}
                {Global.alertContent.message?
                <Alert
                  message={Global.alertContent.status.toUpperCase()}
                  description={Global.alertContent.message}
                  type={Global.alertContent.status}
                  closable={Global.alertContent.closable}
                />:null}       
              </Row>
              <Form onSubmit={this.handleSubmit} className="login-form">
                <fieldset className="auth-form-fieldset">
                  <fieldset className="auth-form-fieldset-input with-margin">
                  <Form.Item>
                  {getFieldDecorator('Email', {
                      rules: [
                          {
                            message: 'Please enter Email',
                              required: true
                          },
                          {
                              max: 35,
                              message: "Email should be accept maximum 35 characters"
                          },
                          {
                            message: "Email should be minimum 3 characters",
                              min: 3,
                          }
                      ],
                    })
                    (<Row type="flex" justify="space-between">
                      <Input className="login-form-input"  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email " size="default" allowClear/></Row>)}
                    </Form.Item>                                   
                  </fieldset>
                  <div className="sign-up-pass-wrapper">
                    <fieldset className="auth-form-fieldset-input">
                    <Form.Item>
                      {getFieldDecorator('Password', {
                          rules: [
                              {
                                message: 'Please enter Password',
                                required: true
                              },
                              {
                                  max: 25,
                                  message: "Password should be accept maximum 25 characters"
                              },
                              {
                                message: "Password should be minimum 8 characters",
                                min: 3
                              }
                          ],
                        })
                        (<Row type="flex" justify="space-between">
                        <Input.Password  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}  className="login-form-input"  placeholder="Password"  />
                        </Row>)}  
                      </Form.Item>    
                    </fieldset>
                    <div className="sign-up-pass-eye"></div>
                  </div>
                  <a onClick={this.clickforgot} href="javascript:void(0);" title="Click to get reset password info" className="login-forget-password">Forgot password?</a>
                </fieldset>
                <Button className="login-btn login-form-btn dark__btn--hover" htmlType="submit">
                  Sign In
                </Button>              
              </Form>
            </div>					
          </div>
          <section className="image-container image-container-login">
            <section className="figures-wrapper figures-wrapper_login">
              <div className="figures-wrapper-container">
                <div className="login-figure first"></div>
                <div className="login-figure second registration_second"></div>
                <div className="login-figure third"></div>
                <div className="login-figure four"></div>
                <div className="login-figure five"></div>
                <div className="login-figure six registration_six"></div>
                <div className="login-figure seven"></div>
              </div>
            </section>
            <div className="container-wrapper">
              <div className="animate-text">
                <div className="container-text-wrapper">
                  <h1 className="image-container-title">Welcome to <span>AutoQ BA</span></h1>
                  <div className="image-container-text">Login and discover a great experience of new features!</div>
                </div>
              </div>
              <div className="image-container-btn white--btn--hover">
                <span >Explore</span>
                <span className="hidden-element">Sign In</span>
              </div>
            </div>
          </section>
          <a href="javascript:void(0);" title="AutoQ BA">
            <div className="page-logo page-logo-login"></div>
          </a>
          <section className="container-flapper"></section>
        </section>                        
      </div>      
  );
  
  }
}

const mapStateToProps = state => ({loginData: state.loginData});

const LoginuserForm = Form.create({ name: 'Register' })(Login);
  
export default connect(mapStateToProps, { loginAction, logoutAction})(LoginuserForm);