import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import jwt from 'jsonwebtoken';
import { Empty } from 'antd';
import Global from '../layout/Global/Global';

class AuthRouter extends Component {
    renderRedirect = () => {
        if (this.props.authNeeded) {
            if(this.props.loginData.token !== null && this.props.loginData.token !== undefined && this.props.loginData.token !== "" && this.props.loginData.token !== "null" ){
                jwt.verify(this.props.loginData.token, '3x6le09r0^p', function (err, decoded) {
                  
                    if (err) {
                        Global.history.push("/login");
                    }
                });
            } else{
                Global.history.push("/login");
            }
               
        }
    }
    render() {
        return (
            <div>
                {this.renderRedirect()}
            </div>
        )
    }
}
const mapStateToProps = state => ({ loginData: state.loginData });

export default connect(mapStateToProps, null)(AuthRouter);