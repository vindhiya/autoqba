import React from "react";
import { Link } from "react-router-dom";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  NavItem,
  NavLink
} from "shards-react";
import {connect} from 'react-redux';

class UserActions extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false
    };

    this.toggleUserActions = this.toggleUserActions.bind(this);
  }

  toggleUserActions() {
    this.setState({
      visible: !this.state.visible
    });
  }

  render() {
    return (
      <NavItem tag={Dropdown} caret toggle={this.toggleUserActions}>
        <DropdownToggle caret tag={NavLink} className="text-nowrap px-3">
          <img
            className="user-avatar rounded-circle mr-2"
            src={require("../../../../images/avatars/avatar-icon.png")}/*{this.props.loginData.avatar}*/
            alt=""
          />{" "}
          {/* neeed to remove static name */}
          <span className="d-none d-md-inline-block">Welcome {this.props.loginData.FirstName} {this.props.loginData.LastName}</span>

        </DropdownToggle>
        <Collapse tag={DropdownMenu} right small open={this.state.visible}>
          <DropdownItem tag={Link} to="user-profile">
            <i className="material-icons">&#xE7FD;</i> Profile
          </DropdownItem>
          <DropdownItem tag={Link} to="/changepassword">
            <i className="material-icons">&#xE8B8;</i>Change Password
          </DropdownItem>
          <DropdownItem divider />
          <DropdownItem tag='a' href="/login" className="text-danger">
            <i className="material-icons text-danger">&#xE879;</i> Logout
          </DropdownItem>
        </Collapse>
      </NavItem>
    );
  }
}
const mapStateToProps = state => ({loginData: state.loginData});
export default connect(mapStateToProps)(UserActions);
