import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout, LoginLayout, LeftNavLayout,ProjectLayout } from "./layouts";

// Route Views
import Dashboard from "./views/Dashboard";
import LoginApp from "./components/login/login";
import Errors from "./views/Errors";
import Addapplication from "./views/addApplication";
import Viewapplication from "./views/viewApplication";
import Editapplication from "./views/editApplication";
import Addproject from "./views/addProject";
import Viewproject from "./views/viewProject";
import Editproject from "./views/editProject";
import Addtool from "./views/addTool";
import Viewtool from "./views/viewTool";
import Edittool from "./views/editTool";
import ForgotPassword from "./components/login/forgotPassword/forgotPassword";
import Adduser from "./views/addUser";
import Viewuser from "./views/viewUser";
import AddRepository from "./views/addRepository";
import ViewRepository from "./views/viewRepository";
import EditRepository from "./views/editRepository";
import ManageRepository from "./views/manageRepository"
import ManageObjects from "./views/manageObjects"
import Addrole from "./views/addRole"
import Viewrole from "./views/viewRole"
import Editrole from "./views/editRole"
import EditRepositoryStore from "./views/editRepositoryStore"
import ViewRepositoryStore from "./views/viewRepositoryStore"
import AddRepositoryStore from "./views/addRepositoryStore"
import Addprivilege from "./views/addPrivilege"
import Editprivilege from "./views/editPrivilege"
import Viewprivilege from "./views/viewPrivilege"
import Edituser from "./views/editUser";
import TestRepositoryObject from "./views/testRepositoryObject";
import Changepassword from "./views/changePassword";

import LaunchProject from "./views/launchproject";

export default [
  {
    authNeeded: false,
    component: () => <Redirect to="/login" />,
    exact: true,
    layout: DefaultLayout,
    path: "/"
  },
  {
    authNeeded: false,
    component: LoginApp,
    layout: LoginLayout,
    path: "/login"
  },
  {
    authNeeded: false,
    component: ForgotPassword,
    layout: LoginLayout,
    path: "/forgot"
  },
  {
    component: LaunchProject,
    layout: DefaultLayout,
    path: "/launchproject"
  },
  {
    component: Dashboard,
    layout: ProjectLayout,
    path: "/home"
  },
  {
    component: Errors,
    layout: LeftNavLayout,
    path: "/errors"
  },
  {
    component: Addapplication,
    layout: DefaultLayout,
    path: "/addapplication"
  },
  {
    component: Viewapplication,
    layout: DefaultLayout,
    path: "/viewapplication"
  },
  {
    component: Editapplication,
    layout: DefaultLayout,
    path: "/editapplication"
  },
  {
    component: Addproject,
    layout: DefaultLayout,
    path: "/addproject"
  },
  {
    component: Viewproject,
    layout: DefaultLayout,
    path: "/viewproject"
  },
  {
    component: Editproject,
    layout: DefaultLayout,
    path: "/editproject"
  },
  {
    component: Addtool,
    layout: DefaultLayout,
    path: "/addtool"
  },
  {
    component: Viewtool,
    layout: DefaultLayout,
    path: "/viewtool"
  },
  {
    component: Edittool,
    layout: DefaultLayout,
    path: "/edittool"
  },
  {
    component: Adduser,
    layout: DefaultLayout,
    path: "/adduser"
  },
  {
    component: Viewuser,
    layout: DefaultLayout,
    path: "/viewuser"
  },
  {
    component: AddRepository,
    layout: DefaultLayout,
    path: "/addRepository"
  },
  {
    component: ViewRepository,
    layout: DefaultLayout,
    path: "/viewrepository"
  },
  {
    component: EditRepository,
    layout: DefaultLayout,
    path: "/editrepository"
  },
  {
    component: ManageRepository,
    layout: DefaultLayout,
    path: "/managerepository"
  },
  {
    component: ManageObjects,
    layout: DefaultLayout,
    path: "/manageObjects"
  },
  {
    component: Addrole,
    layout: DefaultLayout,
    path: "/addrole"
  },
  {
    component: Viewrole,
    layout: DefaultLayout,
    path: "/viewrole"
  },
  {
    component: Editrole,
    layout: DefaultLayout,
    path: "/editrole"
  },
  {
    component: EditRepositoryStore,
    layout: DefaultLayout,
    path: "/editrepositorystore"
  },
  {
    component: ViewRepositoryStore,
    layout: DefaultLayout,
    path: "/viewrepositorystore"
  },
  {
    component: AddRepositoryStore,
    layout: DefaultLayout,
    path: "/addrepositorystore"
  },
  {
    component: TestRepositoryObject,
    layout: DefaultLayout,
    path: "/testrepositorystore"
  },
  {
    component: Editprivilege,
    layout: DefaultLayout,
    path: "/editprivilege"
  },
  {
    component: Edituser,
    layout: DefaultLayout,
    path: "/edituser"
  },
  {
    component: Addprivilege,
    layout: DefaultLayout,
    path: "/addprivilege"
  },
  {
    component: Viewprivilege,
    layout: DefaultLayout,
    path: "/viewprivilege"
  },
  {
    component: TestRepositoryObject,
    layout: DefaultLayout,
    path: "/testrepositoryobject"
  },
  {
    component: Changepassword,
    layout: DefaultLayout,
    path: "/changepassword"
  },
];
