import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "shards-react";

import MainNavbar from "../components/layout/MainNavbar/MainNavbar";
import MainSidebar from "../components/layout/MainSidebar/MainSidebar";
import AuthRouter from '../components/router/AuthRouter'

const DefaultLayout = ({ children, noNavbar, authNeeded}) => (
  <Row>
    <AuthRouter authNeeded={authNeeded}/>
    {!noNavbar && <MainNavbar />}
    <Container fluid className="wrapper-container">
      <Col
        className="main-content"
        tag="main"
      >
        {children}
      </Col>
    </Container>
  </Row>
);

DefaultLayout.propTypes = {
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool,
  /**
   * Whether to display the navbar, or not.
   */
  noNavbar: PropTypes.bool
};

DefaultLayout.defaultProps = {
  noFooter: false,
  noNavbar: false
};

export default DefaultLayout;
