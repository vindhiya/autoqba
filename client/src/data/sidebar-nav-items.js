export default function() {
  return [
    {
      htmlBefore: '<i class="material-icons">note_add</i>',
      title: "Get Quote",
      to: "/home",
    },
    {
      htmlBefore: '<i class="material-icons">table_chart</i>',
      title: "Find Policies",
      to: "/tables",
    }
  ];
}
